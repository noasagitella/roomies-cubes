package com.nse.roomiescubes;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.nse.roomiescubes.board.game.BoardMainGameActivity;
import com.nse.roomiescubes.board.opening.BoardCreateGameActivity;
import com.nse.roomiescubes.player.game.PlayerMainGameActivity;
import com.nse.roomiescubes.player.opening.PlayerStartGameActivity;
import com.nse.roomiescubes.player.opening.PlayerWaitForInvitation;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.util.List;


public class OpeningActivity extends BaseActivity implements View.OnClickListener {

    private Button buttonBoard_;

    private Button buttonInstructions_;

    private Button buttonPlayer_;
    private Button buttonPlayerLogOut_;


    ParseUser currentUser;
    String currentUserName = null;
    Boolean currentPlayerAvailable;
    ParseInstallation installation;
    private TextView hiPlayer_;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_opening);

        buttonBoard_ = (Button) findViewById(R.id.b_board);
        buttonBoard_.setOnClickListener(this);

        buttonInstructions_ = (Button) findViewById(R.id.b_instructions);
        buttonInstructions_.setOnClickListener(this);

        hiPlayer_ = (TextView) findViewById(R.id.tv_hi_player);

        buttonPlayer_ = (Button) findViewById(R.id.b_player);
        buttonPlayer_.setOnClickListener(this);

        buttonPlayerLogOut_ = (Button) findViewById(R.id.b_player_log_out);
        buttonPlayerLogOut_.setOnClickListener(this);

        if(!isNetworkAvailable()) {
            Toast.makeText(getApplicationContext(), "Error - No internet connection", Toast.LENGTH_LONG).show();
        }
        installation = ParseInstallation.getCurrentInstallation();
        currentUser = ParseUser.getCurrentUser();

        if (currentUser != null) {
            currentUserName = currentUser.getUsername();
            ParseQuery query = ParseUser.getQuery();
            query.whereEqualTo("username", currentUserName);
            query.findInBackground(new FindCallback<ParseUser>() {
                public void done(List<ParseUser> objects, ParseException e) {
                    if (e == null) {
                        buttonBoard_.setVisibility(View.VISIBLE);
                        buttonPlayer_.setVisibility(View.VISIBLE);
                        buttonInstructions_.setVisibility(View.VISIBLE);
                        if (objects.isEmpty()) {
                            currentPlayerAvailable = false;
                            buttonPlayerLogOut_.setVisibility(View.GONE);

                            return;
                        } else if ((boolean) objects.get(0).get("logged_in") == true) {
                            currentPlayerAvailable = false;
                            buttonPlayerLogOut_.setVisibility(View.GONE);
                            return;
                        } else {
                            hiPlayer_.setText("Hi " + currentUser.getUsername() + "!");
                            buttonPlayerLogOut_.setVisibility(View.VISIBLE);
                            currentPlayerAvailable = true;
                        }
                    } else {
                        Log.d("Player_LogIn", "not saved" + e.getMessage());
                    }

                    if (ApplicationData.testingMode) {
                        buttonBoard_.setText("Test Board");
                        buttonPlayer_.setText("Test Player");
                        buttonPlayerLogOut_.setVisibility(View.GONE);
                        hiPlayer_.setText("Testing Mode");
                    }

                }
            });
        } else {
            currentPlayerAvailable = false;
            buttonPlayerLogOut_.setVisibility(View.GONE);
            buttonBoard_.setVisibility(View.VISIBLE);
            buttonPlayer_.setVisibility(View.VISIBLE);
            buttonInstructions_.setVisibility(View.VISIBLE);
        }

        if (ApplicationData.testingMode) {
            buttonBoard_.setText("Test Board");
            buttonPlayer_.setText("Test Player");
            buttonPlayerLogOut_.setVisibility(View.GONE);
            hiPlayer_.setText("Testing Mode");
        }


    }



    @Override
    public void onClick(View v) {
        Intent intent;
        Integer id = v.getId();
        switch (id) {
            case R.id.b_instructions:
                intent = new Intent(this, InstructionsActivity.class);
                startActivity(intent);
                break;
            case R.id.b_board: //User chose this device to be the board
                disableButtons();

                ApplicationData.playerType = "board";
                Log.d("Opening activity", "current installation id is: " + installation.getInstallationId());
                ApplicationData.gameId = installation.getInstallationId();
                installation.put("roleType", "board");
                installation.put("gameName", "");
                installation.saveInBackground(new SaveCallback() {
                    public void done(ParseException e) {
                        if (e == null) {
                            Log.d("Opening activity", "saved successfully in installation");
                        } else {
                            Log.d("Opening activity", "not saved in installation. error is: " + e.getMessage());
                        }
                    }
                });
                intent = new Intent(this, BoardCreateGameActivity.class);
                if (ApplicationData.testingMode) {
                    intent = new Intent(this, BoardMainGameActivity.class);
                }
                startActivity(intent);
                enableButtons();
                //finish();
                break;

            case R.id.b_player:

                disableButtons();
                ApplicationData.playerType = "player";
                if (ApplicationData.testingMode) {
                    intent = new Intent(this, PlayerMainGameActivity.class);
                    startActivity(intent);
                    return;
                }
                if (currentPlayerAvailable == true) { //If current user is not null and player clicked on "buttonPlayerSpecific" that
                    ParseQuery query = ParseUser.getQuery();
                    query.whereEqualTo("username", currentUserName);
                    query.findInBackground(new FindCallback<ParseUser>() {
                        public void done(List<ParseUser> objects, ParseException e) {
                            if (e == null) {
                                if (objects.isEmpty()) {
                                    Toast.makeText(getApplicationContext(), "Player " + currentUserName + " does not exist", Toast.LENGTH_SHORT).show();
                                    enableButtons();
                                    return;
                                } else if ((boolean) objects.get(0).get("logged_in") == true) {
                                    Toast.makeText(getApplicationContext(), "Player " + currentUserName + " is already playing", Toast.LENGTH_SHORT).show();
                                   enableButtons();
                                    return;
                                } else {
                                    objects.get(0).put("logged_in", true);
                                    objects.get(0).saveInBackground(new SaveCallback() {
                                        public void done(ParseException e) {
                                            if (e == null) {
                                                Log.d("opening activity", "saved successfully in user");
                                                //means that he wants to log in as the current user
                                                Log.d("Opening activity", "current player is not null");
                                                Log.d("Opening activity", "already logged in");


                                                Log.d("Opening activity", "current installation id is: " + installation.getInstallationId());

                                                Log.d("Opening activity", "Current user is: " + currentUser.getUsername());
                                                Log.d("Opening activity", "Current user is: " + currentUser.getUsername());
                                                installation.put("userID", currentUser); //Update user column in installation object
                                                installation.put("userName", currentUser.getUsername()); //Update user column in installation object

                                                installation.put("gameName", "");
                                                installation.saveInBackground(new SaveCallback() {
                                                    public void done(ParseException e) {
                                                        if (e == null) {
                                                            Log.d("Opening activity", "saved successfully in installation");
                                                        } else {
                                                            Log.d("Opening activity", "not saved in installation. error is: " + e.getMessage());
                                                            Toast.makeText(getApplicationContext(), "There was a connection problem" +
                                                                    ", please try again ", Toast.LENGTH_SHORT).show();
                                                            enableButtons();
                                                            return;
                                                        }
                                                    }
                                                });

                                                ApplicationData.playerName = currentUser.getUsername();
                                                goToNextActivityWitForInvitation();
                                            } else {
                                                Log.d("Opening activity", "not saved in installation. error is: " + e.getMessage());
                                                Toast.makeText(getApplicationContext(), "There was a connection problem" +
                                                        ", please try again ", Toast.LENGTH_SHORT).show();
                                                enableButtons();
                                                return;
                                            }
                                        }
                                    });

                                }
                            } else {
                                Log.d("Opening activity", "quering failed" + e.getMessage());
                                Log.d("Opening activity", "not saved in installation. error is: " + e.getMessage());
                                Toast.makeText(getApplicationContext(), "There was a connection problem" +
                                        ", please try again ", Toast.LENGTH_SHORT).show();
                                enableButtons();
                                return;
                            }
                            enableButtons();

                        }
                    });


                } else {     //If current user is NULL - player needs to choose if he wants to log in or sign up
                    installation.put("gameName", "");
                    installation.saveInBackground(new SaveCallback() {
                        public void done(ParseException e) {
                            if (e == null) {
                                Log.d("Opening activity", "saved successfully in installation");
                            } else {
                                Log.d("Opening activity", "not saved in installation. error is: " + e.getMessage());
                            }
                        }
                    });

                    goToNextActivityPlayerStartGame();
                }
                break;

            case R.id.b_player_log_out:

                disableButtons();
                ParseUser.logOut();
                ApplicationData.playerType = "player";
                //If player clicked on this button that
                //means that this installation has a current user but the player wants to use a different user
                installation = ParseInstallation.getCurrentInstallation();

                installation.put("gameName", "");
                installation.saveInBackground(new SaveCallback() {
                    public void done(ParseException e) {
                        if (e == null) {
                            Log.d("Opening activity", "saved successfully in installation");
                        } else {
                        }
                    }
                });
                goToNextActivityPlayerStartGame();
                break;
            default:
                break;
        }
    }

    private void goToNextActivityPlayerStartGame() {

        Intent intent = new Intent(this, PlayerStartGameActivity.class);
        startActivity(intent);
        enableButtons();
    }

    private void goToNextActivityWitForInvitation() {


        Intent intent = new Intent(this, PlayerWaitForInvitation.class);
        startActivity(intent);
        enableButtons();
    }

    @Override
    public void onDestroy() {
        if (currentUser != null) {
            currentUser.put("logged_in", false);
            currentUser.saveInBackground();
        }
        ApplicationData.leaveGame();
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        if (currentUser != null) {
            currentUser.put("logged_in", false);
            currentUser.saveInBackground();
        }
        ApplicationData.leaveGame();
        onStop();
    }


    private void disableButtons() {
        buttonBoard_.setClickable(false);
        buttonPlayerLogOut_.setClickable(false);
        buttonPlayer_.setClickable(false);
        buttonInstructions_.setClickable(false);
    }


    private void enableButtons() {
        buttonBoard_.setClickable(true);
        buttonPlayerLogOut_.setClickable(true);
        buttonPlayer_.setClickable(true);
        buttonInstructions_.setClickable(true);
    }
}
