package com.nse.roomiescubes.components;

import android.content.Context;
import android.util.Log;
import android.view.DragEvent;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.nse.roomiescubes.R;
import com.nse.roomiescubes.components.card.PlayerCard;

/**
 * Created by Sagit on 06/08/2015.
 */

class Target extends LinearLayout {


    public Target(Context context) {
        this(context, 100, 140);
    }


    public Target(Context context, int width, int height) {
        super(context);
        LinearLayout.LayoutParams layoutParams = super.generateDefaultLayoutParams();
        layoutParams.weight = 1;
        layoutParams.gravity = Gravity.CENTER;

        layoutParams.width = width;
        layoutParams.height = height;

        super.setLayoutParams(layoutParams);
        setBackgroundResource(R.drawable.target);
        setOnDragListener(new TargetDragListener());
    }


    @Override
    public void addView(View child) {
        if (child.getParent() != null) {
            ((LinearLayout) child.getParent()).removeView(child);
        }
        super.addView(child, 0);
    }

    public void removeView() {
        if (super.getChildCount() != 0) {
            super.removeView(super.getChildAt(0));
        }
    }

    public View getView() {
        return super.getChildAt(0);
    }


    /**
     * DragListener will handle dragged views being dropped on the drop area
     * - only the drop action will have processing added to it as we are not
     * - amending the default behavior for other parts of the drag process
     */
    private class TargetDragListener implements OnDragListener {

        @Override
        public boolean onDrag(View v, DragEvent event) {
            //handle the dragged view being dropped over a drop view
            View card = (View) event.getLocalState();
            ViewGroup target = (ViewGroup) v;
            switch (event.getAction()) {
                case DragEvent.ACTION_DRAG_STARTED:
                    card.setVisibility(View.INVISIBLE);
                    break;
                case DragEvent.ACTION_DRAG_ENTERED:
                    if (target.getChildCount() == 0) {
                        target.setBackgroundResource(R.drawable.target_entered);
                    }
                    break;
                case DragEvent.ACTION_DRAG_EXITED:
                    if (target.getChildCount() == 0) {
                        target.setBackgroundResource(R.drawable.target);
                    }
                    break;
                case DragEvent.ACTION_DROP:
                    Log.d("Target", "onDrag.ACTION_DROP");

                    // checking whether target is ok (doesn't have a card)
                    LinearLayout source = (LinearLayout) card.getParent();

                    if (target.getChildCount() == 0) {
                        // update the previous spot
                        source.removeView(card);
                        // update the target spot
                        target.addView(card);
                    }


                    if (card instanceof PlayerCard && target == source) {
                        ((PlayerCard) card).changeSelection();
                    }
                    card.setVisibility(View.VISIBLE);
                    break;
                case DragEvent.ACTION_DRAG_ENDED:
                    v.setBackgroundResource(R.drawable.target);
                    break;
                default:
                    break;
            }
            return true;
        }
    }
}
