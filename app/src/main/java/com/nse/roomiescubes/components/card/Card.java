package com.nse.roomiescubes.components.card;

import android.content.ClipData;
import android.content.Context;
import android.graphics.PorterDuff;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;

import java.util.Comparator;

/**
 * Created by t-sashap on 12-06-15.
 */
public abstract class Card extends ImageView {

    public static final Comparator<Card> cardColorThanValueComparator = new CardColorThanValueComparator();
    public static final Comparator<Card> cardValueThanColorComparator = new CardValueThanColorComparator();

    private CardData cardData_;

    public Card(Context context, CardData cardData) {
        super(context);
        cardData_ = cardData;
        displayCard();
        setOnTouchListener(new CardTouchListener());
    }

    public Card(Context context, Color color, Value value) {
        this(context, new CardData(color, value));
    }

    public void displayCard() {
        setImageResource(cardData_.getImage());
        setVisibility(View.VISIBLE);

    }

    public CardData getCardData() {
        return cardData_;
    }

    public Color getColor() {
        return cardData_.getColor();
    }

    public Value getValue() {
        return cardData_.getValue();
    }

    public int getNumericValue() {
        return cardData_.getNumericValue();
    }

    protected void addColorFilterToCard(int colorFilter) {
        setColorFilter(colorFilter, PorterDuff.Mode.MULTIPLY);
    }

    /**
     * CardTouchListener will handle touch events on draggable views
     */
    private final class CardTouchListener implements View.OnTouchListener {
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
            /*
             * Drag details: we only need default behavior
             * - clip data could be set to pass data as part of drag
             * - shadow can be tailored
             */
                ClipData data = ClipData.newPlainText("", "");
                View.DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(view);
                //start dragging the item touched
                view.startDrag(data, shadowBuilder, view, 0);
                return true;
            } else {
                return false;
            }
        }
    }

    public static class CardColorThanValueComparator implements Comparator<Card> {
        @Override
        public int compare(Card card1, Card card2) {
            int colorCompareResult = Color.colorComparator.compare(card1.getColor(), card2.getColor());
            if (colorCompareResult == 0) {
                return Value.valueComparator.compare(card1.getValue(), card2.getValue());
            }
            return colorCompareResult;
        }
    }

    public static class CardValueThanColorComparator implements Comparator<Card> {
        @Override
        public int compare(Card card1, Card card2) {
            if (card1.getColor() == Color.JOKER || card2.getColor() == Color.JOKER) {
                // if one of the cards is a joker - sort by color - so the joker cards are first
                return Card.cardColorThanValueComparator.compare(card1, card2);
            }
            int valueCompareResult = Value.valueComparator.compare(card1.getValue(), card2.getValue());
            if (valueCompareResult == 0) {
                return Color.colorComparator.compare(card1.getColor(), card2.getColor());
            }
            return valueCompareResult;
        }
    }

    public String toString() {
        return "Card is: " + getColor() + " " + getNumericValue();
    }
}



