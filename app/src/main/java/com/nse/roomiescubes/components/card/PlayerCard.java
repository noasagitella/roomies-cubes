package com.nse.roomiescubes.components.card;

import android.content.ClipData;
import android.content.Context;
import android.support.v4.view.GestureDetectorCompat;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

/**
 * Created by Sagit on 04/08/2015.
 * <b>PlayerCard</b> is created as an <b>un-selected</b> player card.
 * <p/>
 */
public class PlayerCard extends Card {

    private boolean selected_ = false;
    private GestureDetectorCompat gestureDetectorCompat_;
    protected static final int selectColor = android.graphics.Color.rgb(150, 150, 150);

    public PlayerCard(Context context, CardData cardData) {
        super(context, cardData);
        clearColorFilter();
        setOnTouchListener(new PlayerCardTouchListener());
        gestureDetectorCompat_ = new GestureDetectorCompat(this.getContext(), new SingleTapGestureListener());
    }

    public PlayerCard(Context context, Color color, Value value) {
        this(context, new CardData(color, value));
    }

    public boolean isPlayerCardSelected() {
        return selected_;
    }

    public void changeSelection() {
        if (isPlayerCardSelected()) {
            selected_ = false;
            clearColorFilter();
        } else {
            selected_ = true;
            addColorFilterToCard(selectColor);
        }
    }

    private class SingleTapGestureListener extends GestureDetector.SimpleOnGestureListener {
        @Override
        public boolean onDown(MotionEvent event) {
            Log.d("MyGestureListener", "onDown: " + event.toString());
            return true;
        }

        @Override
        public boolean onSingleTapUp(MotionEvent event) {
            Log.d("MyGestureListener", "onSingleTapUp: " + event.toString());
            changeSelection();
            return true;
        }
    }

    private final class PlayerCardTouchListener implements View.OnTouchListener {

        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            Log.d("PlayerCardTouchListener", "onTouch");
            ((PlayerCard) view).gestureDetectorCompat_.onTouchEvent(motionEvent);

            // This is not a single tap
            if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                Log.d("PlayerCardTouchListener", "onTouch - MotionEvent.ACTION_DOWN");
            /*
             * Drag details: we only need default behavior
             * - clip data could be set to pass data as part of drag
             * - shadow can be tailored
             */
                ClipData data = ClipData.newPlainText("", "");
                View.DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(view);
                //start dragging the item touched
                view.startDrag(data, shadowBuilder, view, 0);

                return true;
            } else {
                return false;
            }
        }
    }
}