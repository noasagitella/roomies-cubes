package com.nse.roomiescubes.components.card;

import java.util.Comparator;

/**
 * Created by Sagit on 04/08/2015.
 */
public enum Value {
    ONE(1), TWO(2), THREE(3), FOUR(4), FIVE(5), SIX(6), SEVEN(7), EIGHT(8), NINE(9), TEN(10), ELEVEN(11), TWELVE(12), THIRTEEN(13);

    public final static int JokerValue = 30;
    private final int value_;
    public static final Comparator<Value> valueComparator = new ValueComparator();

    Value(int value) {
        value_ = value;
    }

    public int getValue() {
        return value_;
    }

    private static class ValueComparator implements Comparator<Value> {
        @Override
        public int compare(Value value1, Value value2) {
            return value1.ordinal() - value2.ordinal();
        }
    }
}
