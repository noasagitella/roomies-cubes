package com.nse.roomiescubes.components;

import android.util.Log;

import com.nse.roomiescubes.components.card.CardData;
import com.nse.roomiescubes.components.card.Color;
import com.nse.roomiescubes.components.card.Value;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by t-elless on 04-Aug-15.
 */
public class Deck {
    List<CardData> cards_;

    public Deck() {
        cards_ = new ArrayList<>();
        //creates a list of cards as a deck
        for (Color color : Color.values()) {
            for (int value = 1; value <= color.getRange(); value++) {
                cards_.add(new CardData(color, Value.values()[value - 1]));
                if (color != Color.JOKER) {
                    cards_.add(new CardData(color, Value.values()[value - 1]));
                }
            }
        }

        Collections.shuffle(cards_);
    }

    public int size() {
        return cards_.size();
    }

    public Boolean isEmpty() {
        return (size() == 0);
    }

    public CardData takeCard() {
        Log.d("Deck", "takeCard");
        //TODO decide whether throws if deck is empty
        CardData cardData;
        if (size() > 0) {
            cardData = cards_.get(0);
            cards_.remove(0);
            return cardData;
        }
        throw new UnsupportedOperationException("Not implemented");
    }

    /**
     * @param k is the number of cards wanted.
     *          if there are less then k cards in the deck, it will return those cards.
     * @return k or less cards - depends on the size of the deck.
     */
    public List<CardData> takeKOrLessCards(int k) {
        Log.d("Deck", "takeKOrLessCards");
        List<CardData> cardsDrawn = new ArrayList<>();
        if (size() > 0) {
            int numberOfCards;
            if (size() > k) {
                numberOfCards = k;
            } else {
                numberOfCards = size();
            }
            for (int i = 0; i < numberOfCards; i++) {
                cardsDrawn.add(takeCard());
            }
        }
        return cardsDrawn; // If deck is empty - return empty list
    }

    /**
     * @param k is the number of cards wanted.
     * @return k cards.
     */
    //TODO throws if there less then k cards in the deck.
    public List<CardData> takeKCards(int k) {
        Log.d("Deck", "takeKCards");
        if (size() >= k) {
            return takeKOrLessCards(k);
        }
        throw new UnsupportedOperationException("Throw Empty Deck");
    }

    // TODO i dont we need this
    public List<CardData> DeckToList() {
        Log.d("Deck", "DeckToList");
        //TODO make sure this was Sagit's intention
        List<CardData> listOfCards = new ArrayList<>(cards_);
        return listOfCards;
    }

    /**
     * This method is called when player leaves game and his cards should be returned to deck
     *
     * @param cards is the list of cards we want to return to deck.
     * @return k cards.
     */
    public void returnPlayerCardsToDeck(List<CardData> cards) {
        cards_.addAll(cards);
    }
}
