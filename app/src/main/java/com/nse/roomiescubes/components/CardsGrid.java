package com.nse.roomiescubes.components;

import android.content.Context;
import android.util.Log;
import android.widget.GridLayout;
import android.widget.LinearLayout;

import com.nse.roomiescubes.components.card.Card;
import com.nse.roomiescubes.components.card.Color;
import com.nse.roomiescubes.components.card.Value;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import static java.util.Collections.reverse;

/**
 * Created by Sagit on 06/08/2015.
 */


public class CardsGrid {
    // TODO possible name: CardsDisplay
    // TODO -very unlikely- but if we want a grid of something that's not cards - replace Card with View
    private GridLayout grid_;
    private int numOfRows_;
    private int numOfCols_;
    public static int boardTargetWidth = 100;
    public static int boardTargetHeight = 140;
    int playerTargetWidth = 130;
    int playerTargetHeight = 190;


    private LinearLayout gridLL_;


    /**
     * Fill the grid with Target Objects
     *
     * @param context
     * @param grid    the grid to be filled
     */
    public CardsGrid(Context context, GridLayout grid, GameType gameType) {
        grid_ = grid;
        numOfRows_ = grid_.getRowCount();
        numOfCols_ = grid_.getColumnCount();
        int height;
        int width;
        if (gameType == GameType.BOARD) {
            height = boardTargetHeight;
            width = boardTargetWidth;
        } else {
            height = playerTargetHeight;
            width = playerTargetWidth;
        }
        for (int i = 0; i < size(); i++) {
            grid_.addView(new Target(context, width, height));
        }
    }

    // TODO this is for Sagit auto fit screen testing DO NOT DELETE
    /**
     * Fill the grid with Target Objects
     * @param context
     * @param grid the grid to be filled
     */
//    public CardsGrid(Context context, GridLayout grid) {
//        grid_ = grid;
//        numOfRows_ = grid_.getRowCount();
//        numOfCols_ = grid_.getColumnCount();
//
//        DisplayMetrics displayMetrics = new DisplayMetrics();
//        ((Activity)context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
//
//        int llWidth = displayMetrics.widthPixels;
//        int llHeight = displayMetrics.heightPixels;
//        int targetWidth = llWidth/numOfCols_;
//        int targetHeight = llHeight /  numOfRows_;
//        Log.d("gridLL_", "llWidth: " +llWidth +" llHeight: " +llHeight );
//        Log.d("grid_", "numOfCols_: " +numOfCols_ +" numOfRows_: " +numOfRows_ );
//        Log.d("target", "targetWidth: " +targetWidth +" targetHeight: " +targetHeight );
//
//        for (int i = 0; i < size(); i++) {
//            grid_.addView(new Target(context, targetWidth ,targetHeight ));
//        }
//    }


    /**
     * @return the amount of Target Objects in the grid
     */
    public int size() {
        return numOfRows_ * numOfCols_;
    }

    /**
     * @return index of an empty spot on the grid
     */
    public int getEmptyIndex() {
        for (int i = 0; i < size(); i++) {
            Target target = (Target) grid_.getChildAt(i);
            if (target.getChildCount() == 0) {
                return i;
            }
        }

        // TODO throw new GridIsFullException();
        throw new UnsupportedOperationException("CardsGrid is full");
    }

    /**
     * Adds the card at a empty spot on the grid
     *
     * @param card
     * @throw GridIsFullException
     */
    public void addCard(Card card) {
        // TODO error handling if no empty spot
        Target target = (Target) grid_.getChildAt(getEmptyIndex());
        target.addView(card);
    }

    public void addCard(Card card, int index) {
        // TODO null check?
        // todo index in range
        // todo target is empty check
        Target target = (Target) grid_.getChildAt(index);
        target.addView(card);
    }

    public void removeCard(int index) {
        Target target = (Target) grid_.getChildAt(index);
        target.removeView(); // TODO make sure this is the right index
    }

    public void removeCard(Card card, int index) {
        Target target = (Target) grid_.getChildAt(index);
        target.removeView(card);
    }

    public Card[] asArray() {
        Card[] cards = new Card[size()];
        for (int i = 0; i < size(); i++) {
            Target target = (Target) grid_.getChildAt(i);
            if (target.getChildCount() != 0) {
                cards[i] = (Card) target.getView();
            } else { // TODO maybe no need for else- since we created a new empty array
                cards[i] = null;
            }
        }
        return cards;
    }

    public void updateGrid(Card[] cards) {
        // cards.length > size() : too many cards
        // cards.length < size() : less cards than spots on the grid - will not clean...
        if (cards.length != size()) {   // TODO maybe create a CardsGridArray class matching the size
            // TODO throw new
            throw new UnsupportedOperationException("Cards array does not match to grid in size");
        }
        for (int i = 0; i < cards.length; i++) {
            removeCard(i);
            if (cards[i] != null) {
                addCard(cards[i], i);
            }
        }
    }

    /**
     * cardsListToArray returns an array of grid size, of the cards in the list
     *
     * @param cards List of cards, in size <= grid size
     * @return An array of grid size, with the cards in the list first then null's.
     * @throws
     */
    public Card[] cardsListToArray(List<Card> cards) {
        if (cards.size() > size()) {
            // TODO
            throw new UnsupportedOperationException("Too many cards to the size of the grid");
        }
        Card[] cardsArray = new Card[size()];
        int i = 0;
        while (i < cards.size()) {
            cardsArray[i] = cards.get(i++);
        }
        while (i < size()) {
            cardsArray[i++] = null;
        }
        return cardsArray;
    }

    public List<Card> getCardsList() {
        List<Card> cardsList = new ArrayList<>();
        Card[] cards = this.asArray();
        for (int i = 0; i < cards.length; i++) {
            if (cards[i] != null) {
                cardsList.add(cards[i]);
            }
        }
        return cardsList;
    }

    public List<Card[]> getCardSets() {
        List<Card[]> cardSets = new ArrayList<>();
        Card[] cards = this.asArray();

        for (int i = 0; i < cards.length; i++) {
            if (cards[i] != null) {
                // find next null
                int j = i + 1;
                while (j < cards.length && cards[j] != null) {
                    j++;
                }
                Log.d("cardGroup size: ", (new Integer(j - i).toString()));
                Card[] cardSet = new Card[j - i];
                for (int k = i; k < j; k++) {
                    cardSet[k - i] = cards[k];
                }
                cardSets.add(cardSet);
                i = j;
            }
        }
        Log.d("cardsGroup size: ", (new Integer(cardSets.size()).toString()));

        return cardSets;

    }

    private boolean isSetALegalGroup(List<Card> cardSet) {
        Value groupValue = null;
        Set<Color> colorsInGroup = new TreeSet<Color>();
        for (int i = 0; i < 3; i++) {
            if (cardSet.get(i).getColor() != Color.JOKER) {
                groupValue = cardSet.get(i).getValue();
                break;
            }
        }
        if (groupValue == null) {
            throw new UnsupportedOperationException("Too many jokers!!");
        }

        for (Card card : cardSet) {
            if (card.getColor() == Color.JOKER) {
                continue;
            }
            if (card.getValue() != groupValue) {
                return false;
            }
            //Add color to set. If Color exists this will return false.
            if (!colorsInGroup.add(card.getColor())) {
                return false;
            }
        }
        return true;
    }

    private boolean isSetALegalRun(List<Card> cardSet) {
        // Get game value of first card in Run
        int firstCardValue = -1;
        Color runColor = null;
        for (int i = 0; i < 3; i++) {
            if (cardSet.get(i).getColor() != Color.JOKER) {
                firstCardValue = cardSet.get(i).getNumericValue() - i;
                runColor = cardSet.get(i).getColor();
                break;
            }
        }
        if (runColor == null) {
            throw new UnsupportedOperationException("Too many jokers!!");
        }
        if (firstCardValue <= 0) {
            return false;
        }
        for (int i = 0; i < cardSet.size(); i++) {
            if (cardSet.get(i).getColor() == Color.JOKER) {
                continue;
            }
            if (cardSet.get(i).getNumericValue() != (firstCardValue + i)) {
                return false;
            }
            if (cardSet.get(i).getColor() != runColor) {
                return false;
            }
        }
        return true;
    }


    //TODO: This is only for grid for Board. should extend....
    public Boolean isLegalBoard() {

        Boolean result = true;
        List<Card[]> cardSets = getCardSets();
        for (Card[] cardSet : cardSets) {
            if (cardSet.length < 3) {
                return false;
            }

            List<Card> cardSetList = Arrays.asList(cardSet);
            List<Card> reverseCardSetList = new ArrayList<>(cardSetList);
            reverse(reverseCardSetList);

            if (!isSetALegalGroup(cardSetList) && !isSetALegalRun(cardSetList)
                    && !isSetALegalRun(reverseCardSetList)) {

//               for(Card card: cardSet) {
//                   ((BoardCard) card).markCard();
//                } TODO show marked cards
//
                return false;
            }
        }
        return result;
    }


}
