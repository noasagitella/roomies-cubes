package com.nse.roomiescubes.components.card;

import android.content.Context;

import com.nse.roomiescubes.components.CardsGrid;

/**
 * Created by Sagit on 04/08/2015.
 * <b>BoardCard</b> is created as a <b>new</b> board card,
 * which means it was placed on the board on the current turn and might return
 * to the current player with penalty cards if the "fails" the turn.
 * <p/>
 * <b>isNewCard</b> checks whether the card is new/old.
 * <b>makeOld</b> changes the card to an old card.
 */
public class BoardCard extends Card {
    protected static final int newCardColor = android.graphics.Color.rgb(255, 255, 255);
    protected static final int markCardColor = android.graphics.Color.rgb(255, 0, 255);

    private boolean newCard_ = true;

    public BoardCard(Context context, CardData cardData) {
        super(context, cardData);
        addColorFilterToCard(newCardColor);
    }

    public BoardCard(Context context, Color color, Value value) {
        this(context, new CardData(color, value));
    }

    public boolean isNewCard() {
        return newCard_;
    }

    public void makeOld() {
        newCard_ = false;
        clearColorFilter();
    }

    @Override
    public void displayCard() {
        super.displayCard();
        setAdjustViewBounds(true); // new changes
        setMaxWidth(CardsGrid.boardTargetWidth); // new changes
    }

    public void markCard() {
        addColorFilterToCard(markCardColor);

    }


}
