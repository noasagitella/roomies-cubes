package com.nse.roomiescubes.components.card;

import android.util.Log;

import com.nse.roomiescubes.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sagit on 04/08/2015.
 */
public class CardData {
    private Color color_;
    private Value value_;

    public CardData(Color color, Value value) {
        if (!color.inRange(value.getValue())) {
            //throw new BadCardParams(); //TODO
        }
        color_ = color;
        value_ = value;
    }

    public Color getColor() {
        return color_;
    }

    public Value getValue() {
        return value_;
    }

    public int getNumericValue() {
        if (color_.equals(Color.JOKER)) {
            return Value.JokerValue;
        }
        return value_.getValue();
    }

    public Integer getImage() {
        switch (color_) {
            case JOKER:
                return jokerCards_[value_.getValue() - 1];
            case RED:
                return redCards_[value_.getValue() - 1];
            case BLUE:
                return blueCards_[value_.getValue() - 1];
            case GREEN:
                return greenCards_[value_.getValue() - 1];
            case YELLOW:
                return yellowCards_[value_.getValue() - 1];
            default:
                throw new UnsupportedOperationException("BadCardData"); //TODO
        }
    }

    public static List<CardData> getBackupCardsFromJSON(JSONObject json) {
        List<CardData> cardsData = new ArrayList<CardData>();

        try {

            JSONArray colorArray = json.optJSONArray("colorBackup");
            JSONArray valueArray = json.optJSONArray("valueBackup");
            if (colorArray != null) {
                for (int i = 0; i < colorArray.length(); i++) {
                    Color currentColor = Color.valueOf(colorArray.getString(i));
                    Value currentValue = Value.valueOf(valueArray.getString(i));
                    CardData cardData = new CardData(currentColor, currentValue);
                    cardsData.add(cardData);
                }
            } else {
                Color color = Color.valueOf(json.getString("colorBackup"));
                Value value = Value.valueOf(json.getString("valueBackup"));
                CardData cardData = new CardData(color, value);
                cardsData.add(cardData);
            }

        } catch (JSONException var7) {
            Log.d("CardData", "Unexpected JSONException when receiving push data: ");
        }
        return cardsData;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        final CardData other = (CardData) obj;
        if (this.color_ == other.getColor() && this.value_ == other.getValue()) {
            return true;
        } else {
            return false;
        }

    }

    public static List<CardData> getCardsDataListFromJSON(JSONObject json) {
        List<CardData> cardsData = new ArrayList<CardData>();

        try {

            JSONArray colorArray = json.optJSONArray("color");
            JSONArray valueArray = json.optJSONArray("value");
            if (colorArray != null) {
                for (int i = 0; i < colorArray.length(); i++) {
                    Color currentColor = Color.valueOf(colorArray.getString(i));
                    Value currentValue = Value.valueOf(valueArray.getString(i));
                    CardData cardData = new CardData(currentColor, currentValue);
                    cardsData.add(cardData);
                }
            } else {
                Color color = Color.valueOf(json.getString("color"));
                Value value = Value.valueOf(json.getString("value"));
                CardData cardData = new CardData(color, value);
                cardsData.add(cardData);
            }

        } catch (JSONException var7) {
            Log.d("CardData", "Unexpected JSONException when receiving push data: ");
        }
        return cardsData;
    }

    public static CardData getCardDataFromJSON(JSONObject json) {

        try {
            String s_color = json.getString("color");
            String s_value = json.getString("value");
            Color e_color = Color.valueOf(s_color);
            Value e_value = Value.valueOf(s_value);
            CardData cardData = new CardData(e_color, e_value);
            return cardData;
        } catch (JSONException var7) {
            Log.d("receiver", "Unexpected JSONException when receiving push data: ");
        }
        return null;
    }

    public static JSONObject getJSONArrayFromCardsDataList(List<CardData> cardsData) {
        JSONObject data;
        data = new JSONObject();
        try {

            for (CardData card : cardsData) {
                data.accumulate("color", card.getColor().toString());
                data.accumulate("value", card.getValue().toString());
            }
        } catch (JSONException e1) {
            Log.d("COM: sentInitialCards", "JSONException" + e1.getMessage());
        }
        return data;

    }


    public static JSONObject getJSONFromCardData(CardData cardData) {
        JSONObject data;
        data = new JSONObject();
        try {

            data.put("color", cardData.getColor().toString());
            data.put("value", cardData.getValue().toString());
        } catch (JSONException e1) {
            Log.d("COM: sentInitialCards", "JSONException" + e1.getMessage());
        }
        return data;
    }

    // references to our images
    private Integer[] jokerCards_ = {
            R.drawable.tile_j1, R.drawable.tile_j2
    };
    private Integer[] redCards_ = {
            R.drawable.tile_r01, R.drawable.tile_r02,
            R.drawable.tile_r03, R.drawable.tile_r04,
            R.drawable.tile_r05, R.drawable.tile_r06,
            R.drawable.tile_r07, R.drawable.tile_r08,
            R.drawable.tile_r09, R.drawable.tile_r10,
            R.drawable.tile_r11, R.drawable.tile_r12,
            R.drawable.tile_r13
    };
    private Integer[] blueCards_ = {
            R.drawable.tile_b01, R.drawable.tile_b02,
            R.drawable.tile_b03, R.drawable.tile_b04,
            R.drawable.tile_b05, R.drawable.tile_b06,
            R.drawable.tile_b07, R.drawable.tile_b08,
            R.drawable.tile_b09, R.drawable.tile_b10,
            R.drawable.tile_b11, R.drawable.tile_b12,
            R.drawable.tile_b13
    };

    private Integer[] greenCards_ = {
            R.drawable.tile_g01, R.drawable.tile_g02,
            R.drawable.tile_g03, R.drawable.tile_g04,
            R.drawable.tile_g05, R.drawable.tile_g06,
            R.drawable.tile_g07, R.drawable.tile_g08,
            R.drawable.tile_g09, R.drawable.tile_g10,
            R.drawable.tile_g11, R.drawable.tile_g12,
            R.drawable.tile_g13
    };

    private Integer[] yellowCards_ = {
            R.drawable.tile_y01, R.drawable.tile_y02,
            R.drawable.tile_y03, R.drawable.tile_y04,
            R.drawable.tile_y05, R.drawable.tile_y06,
            R.drawable.tile_y07, R.drawable.tile_y08,
            R.drawable.tile_y09, R.drawable.tile_y10,
            R.drawable.tile_y11, R.drawable.tile_y12,
            R.drawable.tile_y13
    };


}
