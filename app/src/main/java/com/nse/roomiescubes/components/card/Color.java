package com.nse.roomiescubes.components.card;

import java.util.Comparator;

/**
 * Created by Sagit on 04/08/2015.
 */
public enum Color {
    JOKER(2), RED(13), BLUE(13), GREEN(13), YELLOW(13);

    private final int range_;
    public static final Comparator<Color> colorComparator = new ColorComparator();

    Color(int value) {
        range_ = value;
    }

    public int getRange() {
        return range_;
    }

    public boolean inRange(int num) {
        return num >= 1 && num <= range_;
    }

    private static class ColorComparator implements Comparator<Color> {

        @Override
        public int compare(Color color1, Color color2) {
            return color1.ordinal() - color2.ordinal();
        }
    }

}
