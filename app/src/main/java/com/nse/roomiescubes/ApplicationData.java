package com.nse.roomiescubes;

/**
 * Created by t-noanu on 8/4/2015.
 */

import android.app.Application;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import com.nse.roomiescubes.board.game.BoardMainGameActivity;
import com.nse.roomiescubes.board.opening.BoardStartGameActivity;
import com.nse.roomiescubes.communication.Communication;
import com.nse.roomiescubes.communication.CommunicationTest;
import com.nse.roomiescubes.communication.ICommunication;
import com.nse.roomiescubes.player.game.PlayerMainGameActivity;
import com.nse.roomiescubes.player.opening.PlayerWaitForInvitation;
import com.parse.Parse;
import com.parse.ParseACL;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParsePush;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.util.LinkedList;
import java.util.List;


public class ApplicationData extends Application {

    public static String gameId;
    public static String playerType = null;
    public static String gameName = "";
    public static String playerName = "";
    public static List<String> playersList = null;
    public static PlayerWaitForInvitation invitationClass = null;
    public static BoardStartGameActivity boardStartGameClass = null;
    public static PlayerMainGameActivity playerMainGameClass = null;
    public static BoardMainGameActivity boardMainGameClass = null;

    public static boolean testingMode = false; // TODO change this to true for testing!
    public static ICommunication communicationClass = new Communication();


    @Override
    public void onCreate() {
        super.onCreate();

        playersList = new LinkedList<>();

        // Enable Local Datastore.
        Parse.enableLocalDatastore(this);

        Parse.initialize(this, "Xu79mLK0X5NIhWOdkqDAfvPZDQdxOZ0FzroiRLPT", "US7Cz8T1fTwYKEWvFia6UNctQKG9dPH0wC0SrJV7");

        ParseACL defaultACL = new ParseACL();

        ParseACL.setDefaultACL(defaultACL, true);

        /*******************Code For Push Notifications - Start********************/
        ParsePush.subscribeInBackground("", new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e == null) {
                    Log.d("com.parse.push", "successfully subscribed to the broadcast channel.");
                } else {
                    Log.e("com.parse.push", "failed to subscribe for push", e);
                }
            }
        });
        /*******************Code For Push Notifications - End********************/

        if (testingMode) {
            communicationClass = new CommunicationTest();
        }
    }


    /*
    This function is called whenever the player leaves the game after he joined it
     */
    private static void playerLeaveGame() {
        ParseInstallation installation = ParseInstallation.getCurrentInstallation();
        ParseUser currentUser = ParseUser.getCurrentUser();
        if (playersList != null) {
            playersList.remove(playerName);
        }
        gameId = null;
        gameName = null;
        playerName = null;
        if (currentUser != null) {
            currentUser.put("logged_in", false);
            currentUser.saveInBackground();
        }

        installation.put("gameName", "");
        installation.put("userName", "");
        installation.put("roleType", "");
        installation.saveInBackground();
    }

    /*
    This function is called whenever the board leaves the game after it created it
     */
    private static void boardLeaveGame() {
        ParseInstallation installation = ParseInstallation.getCurrentInstallation();
        gameId = null;
        gameName = null;
        playersList.clear();
        installation.put("gameName", "");
        installation.put("userName", "");
        installation.put("roleType", "");
        installation.saveInBackground();
    }

    public static void leaveGame() {
        if (ApplicationData.playerType != null) {
            if (ApplicationData.playerType.equals("player")) {
                playerLeaveGame();
            } else {
                boardLeaveGame();
            }
        }
    }

}

