package com.nse.roomiescubes.board.Ending;

import android.app.Activity;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.nse.roomiescubes.ApplicationData;
import com.nse.roomiescubes.OpeningActivity;
import com.nse.roomiescubes.R;

import java.util.ArrayList;


public class BoardGameOverActivity extends Activity implements View.OnClickListener {

    private Button buttonPlayAgain_;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_board_game_over);
        buttonPlayAgain_ = (Button) findViewById(R.id.b_play_again);
        buttonPlayAgain_.setOnClickListener(this);
        TextView winner1 = (TextView) findViewById(R.id.v_winner1);
        TextView winner2 = (TextView) findViewById(R.id.v_winner2);
        ArrayList<String> winnersNames = getIntent().getStringArrayListExtra("winner");
        String msg = new String();
        ApplicationData.leaveGame();
        if (winnersNames.size() == 1) {
            winner1.setText("The winner is:");
            msg = winnersNames.get(0);
        } else {
            for (int i = 0; i < winnersNames.size(); i++) {
                msg.concat(winnersNames.get(i));
                if (i != (winnersNames.size() - 1)) {
                    msg.concat(", ");
                } else {
                    msg.concat("!");
                }
            }
        }

        winner2.setText(msg);
    }


    @Override
    public void onClick(View v) {
        // play again button
        Intent intent = new Intent(this, OpeningActivity.class);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, OpeningActivity.class);
        startActivity(intent);
    }

    @Override
    public void onDestroy() {
        ApplicationData.leaveGame();
        super.onDestroy();
    }

}
