package com.nse.roomiescubes.board.opening;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.GridLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.nse.roomiescubes.ApplicationData;
import com.nse.roomiescubes.OpeningActivity;
import com.nse.roomiescubes.R;
import com.nse.roomiescubes.BaseActivity;
import com.nse.roomiescubes.board.game.BoardMainGameActivity;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.List;

public class BoardStartGameActivity extends BaseActivity {
    private final static int minNumberOfUsers_ = 2;
    private final static int maxNumberOfUsers_ = 4;
    private EditText playerNameToInvite_;
    private Button invitePlayer_;
    private Player[] players_ = new Player[maxNumberOfUsers_];
    private Button startGame_;
    int numberOfUsersInvited_ = 0;
    int numberOfUsersApproved_ = 0;
    List<String> invitedPlayersNames_ = new ArrayList<String>();
    View.OnClickListener clickListener_ = new MyClickListener();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_board_start_game);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.
                SOFT_INPUT_STATE_VISIBLE | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);

        playerNameToInvite_ = (EditText) findViewById(R.id.e_playerName);
        Log.d("BoardStartGameActivity", "game name is" + ApplicationData.gameName);

        for (int i = 0; i < maxNumberOfUsers_; i++) {
            players_[i] = new Player(i);
        }

        invitePlayer_ = (Button) findViewById(R.id.b_invite_player);
        invitePlayer_.setOnClickListener(clickListener_);

        startGame_ = (Button) findViewById(R.id.b_start_game);
        startGame_.setOnClickListener(clickListener_);
        enableOrDisableStartGameButton();

        ApplicationData.boardStartGameClass = this;

        Log.d("BoardStartGameActivity", "inCreate");
        if(!isNetworkAvailable()) {
            Toast.makeText(getApplicationContext(), "Error - No internet connection", Toast.LENGTH_LONG).show();
        }
    }



    DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            switch (which) {
                case DialogInterface.BUTTON_POSITIVE:
                    dialog.dismiss();
                    ApplicationData.communicationClass.BoardToPlayerGameStoppedUnexpectedly(ApplicationData.gameId,
                            ApplicationData.gameName,
                            invitedPlayersNames_);
                    ApplicationData.boardStartGameClass = null;
                    ApplicationData.leaveGame();
                    Intent intent = new Intent(BoardStartGameActivity.this, OpeningActivity.class);
                    startActivity(intent);
                    //finish();

                case DialogInterface.BUTTON_NEGATIVE:
                    dialog.dismiss();
                    break;
            }
        }
    };

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Are you sure you you want to leave the game?").setPositiveButton("Yes", dialogClickListener)
                .setNegativeButton("No", dialogClickListener).show();
    }


    class MyClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            if(!isNetworkAvailable()) {
                Toast.makeText(getApplicationContext(), "Error - No internet connection", Toast.LENGTH_LONG).show();
                return;
            }

            Integer id = v.getId();
            String name;
            switch (id) {
                case R.id.b_invite_player:
                    Log.d("BoardStartGameActivity", "b_invite_player clicked");
                    String playerName = playerNameToInvite_.getText().toString();
                    if (invitedPlayersNames_.contains(playerName)) {
                        Toast.makeText(getApplicationContext(), "You already invited " + playerName, Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if (!playerName.isEmpty()) {
                        if (numberOfUsersInvited_ < maxNumberOfUsers_) {
                            inviteUser(playerName);
                            playerNameToInvite_.setText("");
                            hideSoftKeyboard();
                        } else {
                            Toast.makeText(getApplicationContext(), "You can only invite 4 players", Toast.LENGTH_SHORT).show();
                        }
                    }
                    break;
                case R.id.b_cancel_invitation1:
                    if (id == R.id.b_cancel_invitation1) {
                        updateCanceledUser(players_[0].playerName_.getText().toString());
                    }
                    name = players_[1].playerName_.getText().toString();
                    players_[0].updatePlayer(name, players_[1].isInvited_, players_[1].hasApproved_,
                            players_[1].hasDeclined_);
                    players_[1].resetPlayer();
                    // no break on purpose
                case R.id.b_cancel_invitation2:
                    if (id == R.id.b_cancel_invitation2) {
                        updateCanceledUser(players_[1].playerName_.getText().toString());
                    }
                    name = players_[2].playerName_.getText().toString();
                    players_[1].updatePlayer(name, players_[2].isInvited_, players_[2].hasApproved_,
                            players_[2].hasDeclined_);
                    players_[2].resetPlayer();
                    // no break on purpose
                case R.id.b_cancel_invitation3:
                    if (id == R.id.b_cancel_invitation3) {
                        updateCanceledUser(players_[2].playerName_.getText().toString());
                    }
                    name = players_[3].playerName_.getText().toString();
                    players_[2].updatePlayer(name, players_[3].isInvited_, players_[3].hasApproved_,
                            players_[3].hasDeclined_);
                    players_[3].resetPlayer();
                    // no break on purpose
                case R.id.b_cancel_invitation4:
                    if (id == R.id.b_cancel_invitation4) {
                        updateCanceledUser(players_[3].playerName_.getText().toString());
                    }
                    players_[3].resetPlayer();
                    numberOfUsersInvited_--;
                    updateNumOfApprovedPlayersAfterCancellation();
                    enableOrDisableInviteUserButton();
                    enableOrDisableStartGameButton();
                    break;

                case R.id.b_start_game:
                    startGame();
                    break;
            }

        }
    }

    public void onPlayerJoinsGame(String playerName) {
        ApplicationData.playersList.add(playerName);
        numberOfUsersApproved_++;
        try {
            players_[getPlayerIndex(playerName)].approvePlayer();
        } catch (UnsupportedOperationException e) {
            Log.d("BoardStartGameActivity", "player " + playerName + " joined game but not invited");
        }
        enableOrDisableStartGameButton();
    }

    public void onPlayerDeclinesGame(String playerName) {
        try {
            players_[getPlayerIndex(playerName)].playerDeclined();
        } catch (UnsupportedOperationException e) {
            Log.d("BoardStartGameActivity", "player " + playerName + " declined game but not invited");
        }
    }

    public void onPlayerLeavesGame(String playerName) {
        Log.d("BoardStartGameActivity", "player " + playerName + " left game");
        try {
            players_[getPlayerIndex(playerName)].playerLeft();
            ApplicationData.playersList.remove(playerName);
        } catch (UnsupportedOperationException e) {
            Log.d("BoardStartGameActivity", "player " + playerName + " left game but not invited");
        }

    }


    private void updateCanceledUser(String nameOfCanceledUser) {
        Log.d("BoardStartGameActivity", "updateCanceledUser");
        ApplicationData.communicationClass.BoardToPlayerCancelInvitation(ApplicationData.gameId, ApplicationData.gameName, nameOfCanceledUser);
        invitedPlayersNames_.remove(nameOfCanceledUser);
    }

    private void startGame() {
        Intent intent = new Intent(this, BoardMainGameActivity.class);
        startActivity(intent);
    }


    private void updateNumOfApprovedPlayersAfterCancellation() {
        numberOfUsersApproved_ = 0;
        for (int i = 0; i < players_.length; i++) {
            if (players_[i].hasApproved_) {
                numberOfUsersApproved_++;
            }
        }
    }

    private int getPlayerIndex(String playerName) {
        for (int i = 0; i < players_.length; i++) {
            String name = players_[i].playerName_.getText().toString();
            if (!name.isEmpty()) {
                if (name.equals(playerName)) {
                    return i;
                }
            }
        }
        throw new UnsupportedOperationException("Player with name " + playerName + " was not invited!");
    }


    private void inviteUser(final String name) {
        Log.d("BoardStartGameActivity", "inviteUser called");
        // number of invited users < maxNumberOfUsers_
        final ParseQuery<ParseUser> query = ParseUser.getQuery();
        query.whereEqualTo("logged_in", true);
        query.whereEqualTo("username", name);
        query.findInBackground(new FindCallback<ParseUser>() {
                                   public void done(List<ParseUser> objects, ParseException e) {
                                       if (e == null) {
                                           if (!objects.isEmpty()) {
                                               Toast.makeText(getApplicationContext(), "Inviting the user", Toast.LENGTH_SHORT).show();
                                               ApplicationData.communicationClass.BoardToPlayerInvitePlayer(name, ApplicationData.gameId,
                                                       ApplicationData.gameName);
                                               players_[numberOfUsersInvited_++].invitePlayer(name);
                                               invitedPlayersNames_.add(name);
                                               enableOrDisableInviteUserButton();
                                               enableOrDisableStartGameButton();
                                           } else {
                                               Toast.makeText(getApplicationContext(), "User name " + name + " doesn't exist or isn't logged in", Toast.LENGTH_SHORT).show();
                                           }
                                       } else {
                                           Log.d("Board start game", "Error finding user:" + e.getMessage());
                                       }
                                   }
                               }
        );
    }

    private void enableOrDisableInviteUserButton() {
        if (numberOfUsersInvited_ == maxNumberOfUsers_) {
            invitePlayer_.setEnabled(false);
        } else {
            invitePlayer_.setEnabled(true);
        }
    }

    private void enableOrDisableStartGameButton() {
        if (numberOfUsersApproved_ == numberOfUsersInvited_ && numberOfUsersInvited_ >= minNumberOfUsers_) {
            startGame_.setEnabled(true);
        } else {
            startGame_.setEnabled(false);
        }

    }

    /*
    This class represent an invited player's view object
     */
    class Player {
        public int playerNum_; // serial number of this player
        public GridLayout player_; //GridLayout containing this player's details
        public TextView playerName_; //Player's name
        public Button cancel_; //Button that cancels this player's invitation
        public TextView statusText_; //The text view that contains this player's invitation status
        public boolean isInvited_; // is this view belongs to an invited player (true) or not invited (false)
        public CheckBox checkBox_; // The check box that represents if the player joined the game
        public boolean hasApproved_; //true if the player joined the gamr, false if he didn't
        public boolean hasDeclined_; //true if the player declined the game, false if he didn't

        /**
         * <b>Constructor</b>
         *
         * @param num This view's serial number.
         */
        Player(int num) {
            Log.d("BoardStartGameActivity", "initialize " + playerNum_);
            getDisplayFields(num);
            updateFieldsUnInvitedPlayer();

        }

        /**
         * <b>resetPlayer</b>  -  reset the object to un-invited state when the invitation of the object is canceled
         */
        void resetPlayer() {
            isInvited_ = false;
            hasApproved_ = false;
            hasDeclined_ = false;
            statusText_.setText("waiting for player");
        }

        /**
         * <b>invitePlayer</b>  -  called to change the object to state to "invited"
         *
         * @param name - the player's name to invite
         */
        void invitePlayer(String name) {
            Log.d("BoardStartGameActivity", "invitePlayer " + playerNum_);
            player_.setVisibility(View.VISIBLE);
            playerName_.setText(name);
            cancel_.setVisibility(View.VISIBLE);
            statusText_.setVisibility(View.VISIBLE);
            isInvited_ = true;
            checkBox_.setVisibility(View.GONE);
            hasApproved_ = false;
            hasDeclined_ = false;
            statusText_.setText("waiting for player");
        }

        /**
         * <b>approvePlayer</b>  -  called to change the object to state 'approved' when the
         */
        void approvePlayer() {
            Log.d("BoardStartGameActivity", "approvePlayer " + playerNum_);
            // assumes the player is invited - invitePlayer() was called on this player
            if (!isInvited_ || playerName_.getText().toString().isEmpty()) {
                Log.d("BoardStartGameActivity", "unexpected player joined game");
            }
            statusText_.setVisibility(View.GONE);
            checkBox_.setVisibility(View.VISIBLE);
            hasApproved_ = true;
        }

        /**
         * <b>playerDeclined</b>  -  called to change the object to state 'declined' when the
         * player declined the invitation
         */
        void playerDeclined() {
            Log.d("BoardStartGameActivity", "approvePlayer " + playerNum_);
            // assumes the player is invited - invitePlayer() was called on this player
            if (!isInvited_ || playerName_.getText().toString().isEmpty()) {
                Log.d("BoardStartGameActivity", "unexpected player declined game");
            }
            statusText_.setText("declined invitation");
            checkBox_.setVisibility(View.GONE);
            hasDeclined_ = true;
        }

        /**
         * <b>playerLeft</b>  -  called to change the object to state when the invited
         * player leaves the game
         */
        void playerLeft() {
            Log.d("BoardStartGameActivity", "approvePlayer " + playerNum_);
            // assumes the player is invited - invitePlayer() was called on this player
            if (!isInvited_ || playerName_.getText().toString().isEmpty()) {
                Log.d("BoardStartGameActivity", "unexpected player left game");
            }
            statusText_.setText("not available");
            statusText_.setVisibility(View.VISIBLE);
            checkBox_.setVisibility(View.GONE);
            hasDeclined_ = false;
            hasApproved_ = false;
        }

        /**
         * <b>updatePlayer</b>  -  called to update the view with new invitation state
         *
         * @param name        - the player's name that is invited in this view
         * @param isInvited   - true if this view represent an invited player and false otherwise.
         * @param hasApproved - true if the player invited in this view has joined the game, false otherwise
         * @param hasDeclined - true if the player invited in this view has declined the game, false otherwise
         */
        void updatePlayer(String name, boolean isInvited, Boolean hasApproved, boolean hasDeclined) {
            Log.d("BoardStartGameActivity", "updatePlayer " + playerNum_ + " with player named: " + name + " isInvited: " + isInvited + " hasApproved: " + hasApproved);
            updateFieldsUnInvitedPlayer();
            if (isInvited) {
                invitePlayer(name);
            }
            if (hasApproved) {
                approvePlayer();
            }
            if (hasDeclined) {
                playerDeclined();
            }

        }

        /**
         * <b>updateFieldsUnInvitedPlayer</b>  -  called to change the object to state when to
         * "un invited" state
         */
        private void updateFieldsUnInvitedPlayer() {
            player_.setVisibility(View.GONE);
            playerName_.setText("");
            cancel_.setVisibility(View.VISIBLE);
            cancel_.setOnClickListener(clickListener_);
            statusText_.setVisibility(View.GONE);
            isInvited_ = false;
            checkBox_.setVisibility(View.GONE);
            hasApproved_ = false;
            hasDeclined_ = false;
        }

        /**
         * <b>getDisplayFields</b>  - initialized the object with the view objects
         * connected to it
         */
        private void getDisplayFields(int playerNum) {
            switch (playerNum + 1) {
                case 1:
                    playerNum_ = 1;
                    player_ = (GridLayout) findViewById(R.id.ll_player1);
                    playerName_ = (TextView) findViewById(R.id.tv_player1_name);
                    statusText_ = (TextView) findViewById(R.id.tv_status1);
                    checkBox_ = (CheckBox) findViewById(R.id.check1);
                    cancel_ = (Button) findViewById(R.id.b_cancel_invitation1);

                    break;
                case 2:
                    playerNum_ = 2;
                    player_ = (GridLayout) findViewById(R.id.ll_player2);
                    playerName_ = (TextView) findViewById(R.id.tv_player2_name);
                    statusText_ = (TextView) findViewById(R.id.tv_status2);
                    checkBox_ = (CheckBox) findViewById(R.id.check2);
                    cancel_ = (Button) findViewById(R.id.b_cancel_invitation2);
                    break;

                case 3:
                    playerNum_ = 3;
                    player_ = (GridLayout) findViewById(R.id.ll_player3);
                    playerName_ = (TextView) findViewById(R.id.tv_player3_name);
                    statusText_ = (TextView) findViewById(R.id.tv_status3);
                    checkBox_ = (CheckBox) findViewById(R.id.check3);
                    cancel_ = (Button) findViewById(R.id.b_cancel_invitation3);
                    break;
                case 4:
                    playerNum_ = 4;
                    player_ = (GridLayout) findViewById(R.id.ll_player4);
                    playerName_ = (TextView) findViewById(R.id.tv_player4_name);
                    statusText_ = (TextView) findViewById(R.id.tv_status4);
                    checkBox_ = (CheckBox) findViewById(R.id.check4);
                    cancel_ = (Button) findViewById(R.id.b_cancel_invitation4);
                    break;
                default:
                    break;

            }

        }

    }

    @Override
    public void onDestroy() {
        ApplicationData.leaveGame();
        super.onDestroy();
    }

}


