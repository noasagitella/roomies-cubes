package com.nse.roomiescubes.board.opening;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.nse.roomiescubes.ApplicationData;
import com.nse.roomiescubes.OpeningActivity;
import com.nse.roomiescubes.R;
import com.nse.roomiescubes.BaseActivity;
import com.parse.ParseInstallation;

public class BoardCreateGameActivity extends BaseActivity implements View.OnClickListener {

    private EditText editTextGameName;
    private Button buttonCreateGame;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_board_create_game);
        editTextGameName = (EditText) findViewById(R.id.e_gameName);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);

        buttonCreateGame = (Button) findViewById(R.id.b_createGame);

        buttonCreateGame.setOnClickListener(this);

        Log.d("BoardCreateGameActivity", "inCreate");
        if(!isNetworkAvailable()) {
            Toast.makeText(getApplicationContext(), "Error - No internet connection", Toast.LENGTH_LONG).show();
        }
    }


    @Override
    public void onClick(View v) {
        // create game button
        Log.d("BoardCreateGameActivity", "onClick");
        if(!isNetworkAvailable()) {
            Toast.makeText(getApplicationContext(), "Error - No internet connection", Toast.LENGTH_LONG).show();
            return;
        }

        buttonCreateGame.setClickable(false);


        String newGameName;
        // empty string, don't parse as int.
        if (editTextGameName.getText().toString().isEmpty()) {
            newGameName = null;
            buttonCreateGame.setClickable(true);

        } else {
            // parse as int and update make order.
            newGameName = String.valueOf(editTextGameName.getText().toString());
            ApplicationData.gameName = newGameName;
            ParseInstallation installation = ParseInstallation.getCurrentInstallation();
            installation.put("userName", newGameName);
            installation.put("gameName", newGameName);
            installation.saveInBackground();
            Intent intent = new Intent(this, BoardStartGameActivity.class);
            startActivity(intent);

            buttonCreateGame.setClickable(true);
     }
    }

    @Override
    public void onDestroy() {
        ApplicationData.leaveGame();
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, OpeningActivity.class);
        startActivity(intent);
    }


}
