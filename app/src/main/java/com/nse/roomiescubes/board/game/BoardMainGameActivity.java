package com.nse.roomiescubes.board.game;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.DragEvent;
import android.view.View;
import android.view.View.OnDragListener;
import android.widget.Button;
import android.widget.GridLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.nse.roomiescubes.ApplicationData;
import com.nse.roomiescubes.InstructionsActivity;
import com.nse.roomiescubes.OpeningActivity;
import com.nse.roomiescubes.R;
import com.nse.roomiescubes.BaseActivity;
import com.nse.roomiescubes.board.Ending.BoardGameOverActivity;
import com.nse.roomiescubes.components.CardsGrid;
import com.nse.roomiescubes.components.Deck;
import com.nse.roomiescubes.components.GameType;
import com.nse.roomiescubes.components.card.BoardCard;
import com.nse.roomiescubes.components.card.Card;
import com.nse.roomiescubes.components.card.CardData;
import com.nse.roomiescubes.components.card.Color;
import com.nse.roomiescubes.components.card.Value;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class BoardMainGameActivity extends BaseActivity implements View.OnClickListener {

    /**********************************************NOA-FOR TEST - START*************************/
//    private Handler mHandler = new Handler();
//    public void doStuff(CardData cardData,List<CardData> penaltyList) {
//        Communication.BoardToPlayerTurnSuccessfulCardTaken("ima", cardData);
//        Communication.BoardToPlayerInformYourTurn("aba");
//        Communication.BoardToPlayerTurnUnsuccessful("ima", penaltyList);
//        Communication.BoardToPlayerGameOver("ima");
//    }
    /**
     * *******************************************NOA-FOR TEST - START************************
     */


    private ImageButton deckButton_;
    private Button instructions_;
    private Button endTurnButton_;
    private Button finishGame_;
    private LinearLayout terminal_;
    private LinearLayout background_;
    private TextView playerTurnDisplay_;
    private BoardTimer boardTimer_;

    private GameData gameData_;
    private CardsGrid cardsGrid_;

    private boolean endGameClicked_ = false;
    //For testing
    private Button testPlaceNewCards_;


    /* Is called to start activity */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d("BoardMainGameActivity", "onCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_board_main_game);

        getMemberViewsAndSetListeners();
        endTurnButton_.setEnabled(false);
        deckButton_.setEnabled(true);

        Button timer = (Button) findViewById(R.id.et_timer);
        timer.setOnClickListener(this);
        boardTimer_ = new BoardTimer(timer);

        cardsGrid_ = new CardsGrid(this, (GridLayout) findViewById(R.id.gl_board_grid), GameType.BOARD);

        setupGame();
        ApplicationData.boardMainGameClass = this; //TODO
        if(!isNetworkAvailable()) {
            Toast.makeText(getApplicationContext(), "Error - No internet connection", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onResume () {
        super.onResume();
        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
    }

    @Override
    public void onDestroy() {
        ApplicationData.leaveGame();
        super.onDestroy();
    }


    /* Get Ui objects and set listeners */
    protected void getMemberViewsAndSetListeners() {
        Log.d("BoardMainGameActivity", "getMemberViewsAndSetListeners");
        background_ = (LinearLayout) findViewById(R.id.board_main_game_layout);
        background_.setOnDragListener(new MyDragListener());

        playerTurnDisplay_ = (TextView) findViewById(R.id.tv_player_display_turn);

        terminal_ = (LinearLayout) findViewById(R.id.terminal);
        terminal_.setOnDragListener(new MyDragListener());

        instructions_ = (Button) findViewById(R.id.b_instructions);
        instructions_.setOnClickListener(this);

        endTurnButton_ = (Button) findViewById(R.id.b_end_turn);
        endTurnButton_.setOnClickListener(this);

        finishGame_ = (Button) findViewById(R.id.b_finish_game);
        finishGame_.setOnClickListener(this);

        deckButton_ = (ImageButton) findViewById(R.id.ib_deck);
        deckButton_.setOnClickListener(this);

        testPlaceNewCards_ = (Button) findViewById(R.id.b_test_place_cards);
        testPlaceNewCards_.setOnClickListener(this);


    }

    /* Gets players, setups game and siplays players turn*/
    protected void setupGame() {
        Log.d("BoardMainGameActivity", "setupGame");

        List<String> players;
        if (ApplicationData.testingMode) {
            players = new ArrayList<String>();
            players.add("Noa");
            players.add("Sagit");
            players.add("Ella");
            testPlaceNewCards_.setVisibility(View.VISIBLE);
            testPlaceNewCards_.setEnabled(true);
        } else {
            players = ApplicationData.playersList;
        }
        gameData_ = new GameData(players);
        displayPlayersTurn();
        boardTimer_.start();
    }

    /* Display current and next player on board */
    public void displayPlayersTurn() {
        String currentPlayer = (gameData_.playersData_).getCurrentPlayer();
        String nextPlayer = (gameData_.playersData_).getNextPlayer();
        String msg = currentPlayer + "'s turn! Next Player: " + nextPlayer;
        playerTurnDisplay_.setText(msg);
    }


    DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {

        /* This function is called when one of the buttons in the dialog is called*/
        @Override
        public void onClick(DialogInterface dialog, int which) {
            switch (which) {
                /*End Game*/
                case DialogInterface.BUTTON_POSITIVE:
                    dialog.dismiss();
                    boardTimer_.stop();
                    /*End game because end game buttons was clicked */
                    if (endGameClicked_) {
                        gameData_.endGameWithoutEmptyRack();
                    } else { // End game because not enough players (player quit)
                        ApplicationData.communicationClass.BoardToPlayerGameStoppedUnexpectedly(ApplicationData.gameId,
                                ApplicationData.gameName, gameData_.players_);
                        ApplicationData.boardMainGameClass = null;
                        ApplicationData.leaveGame();
                        Intent intent = new Intent(BoardMainGameActivity.this, OpeningActivity.class);
                        startActivity(intent);
                        break;
                    }
                    break;
                /* Don't end game*/
                case DialogInterface.BUTTON_NEGATIVE:
                    dialog.dismiss();
                    break;
            }
        }
    };


    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Are you sure you you want to stop the game?").setPositiveButton("Yes", dialogClickListener)
                .setNegativeButton("No", dialogClickListener).show();
    }


    class MyDragListener implements OnDragListener {
        /**
         * This function is called if a drag event is received by a view that is not part of the cardGrid.
         * <p/>
         * If the events is "drop" - put that card in the terminal.
         * Otherwise - do nothing
         */

        @Override
        public boolean onDrag(View layoutView, DragEvent event) {
            int action = event.getAction();
            LinearLayout dest = (LinearLayout) layoutView;

            switch (action) {
                case DragEvent.ACTION_DRAG_STARTED:
                    // do nothing
                    break;
                case DragEvent.ACTION_DRAG_ENTERED:
                    Log.d("ACTION_DRAG_ENTERED", "x: " + event.getX() + " y: " + event.getY());
                    // do nothing
                    break;
                case DragEvent.ACTION_DRAG_EXITED:
                    Log.d("ACTION_DRAG_EXITED", "x: " + event.getX() + " y: " + event.getY());
                    // do nothing
                    break;
                case DragEvent.ACTION_DROP:
                    Log.d("BoardMainGameActivity", "onDrag.ACTION_DROP");

                    // Dropped, reassign View to ViewGroup
                    View card = (View) event.getLocalState();
                    //runtime casting - this should always succeed
                    if (dest.getId() == R.id.terminal || dest.getId() == R.id.board_main_game_layout) {
                        // update the previous spot
                        LinearLayout source = (LinearLayout) card.getParent();
                        source.removeView(card);
                        // update the target spot
                        terminal_.addView(card);
                    }
                    card.setVisibility(View.VISIBLE);
                    break;
                case DragEvent.ACTION_DRAG_ENDED:
                default:
                    break;
            }
            return true;
        }
    }


    /**
     * This function is called if player sent cards to board.
     * If the player that sent these cards is the current player by board
     * the function creates BoardCards from cardData List and places them in terminal.
     * If the player that sent the cards isn't the current player - does nothing.
     */
    public void receiveCardsFromPlayer(List<CardData> cardsFromPlayer, String playerName) {
        Log.d("BoardMainGameActivity", "receiveCardsFromPlayer");
        Log.d("Current Player: ", gameData_.playersData_.getCurrentPlayer());
        Log.d("playerName from JSON: ", playerName);

        if (playerName.equals(gameData_.playersData_.getCurrentPlayer())) {
            for (CardData cardData : cardsFromPlayer) {
                BoardCard boardCard = new BoardCard(this, cardData);
                terminal_.addView(boardCard);
                deckButton_.setEnabled(false);
                endTurnButton_.setEnabled(true);
            }
            gameData_.receiveCardsFromPlayer(cardsFromPlayer);
        }
    }

    /**
     * This function is called if player exited the game
     * It updates the game data accordingly
     * The game will resume if there are 2 or more players,
     * otherwise it will end.
     *
     * @param playerName - the player that left the game
     */

    public void playerLeavesGame(String playerName) {
        Log.d("BoardMainGameActivity", "player" + playerName + "LeavesGame");
        Toast.makeText(this.getApplicationContext(), "player " + playerName + "is leaving the game", Toast.LENGTH_SHORT).show();
        gameData_.playerLeavesGame(playerName);
    }

    /**
     * Takes care of all the button clicks made in this activity.
     *
     * @param v - the view that was clicked
     */
    @Override
    public void onClick(View v) {
        Log.d("BoardMainGameActivity", "onClick");

        Integer id = v.getId();
        switch (id) {
            case R.id.et_timer: {
                Log.d("BoardMainGameActivity", "OnClick.timer button clicked");
                boardTimer_.click();
                break;
            }
            case R.id.b_instructions: {
                Log.d("BoardMainGameActivity", "OnClick.Instructions button clicked");
                Intent intent = new Intent(BoardMainGameActivity.this, InstructionsActivity.class);
                startActivity(intent);
                break;
            }
            case R.id.ib_deck: {
                if(!isNetworkAvailable()) {
                    Toast.makeText(getApplicationContext(), "Error - No internet connection", Toast.LENGTH_LONG).show();
                    return;
                }
                Log.d("BoardMainGameActivity", "OnClick.Deck clicked");
                boardTimer_.stop();
                gameData_.onDeck();
                boardTimer_.start();
                break;
            }
            case R.id.b_end_turn: {
                if(!isNetworkAvailable()) {
                    Toast.makeText(getApplicationContext(), "Error - No internet connection", Toast.LENGTH_LONG).show();
                    return;
                }
                Log.d("BoardMainGameActivity", "End turn button clicked");
                boardTimer_.stop();
                gameData_.onEndTurnButtonClick();
                boardTimer_.start();
                break;
            }
            case R.id.b_finish_game: {
                if(!isNetworkAvailable()) {
                    Toast.makeText(getApplicationContext(), "Error - No internet connection", Toast.LENGTH_LONG).show();
                    return;
                }
                Log.d("BoardMainGameActivity", "Finish game button clicked");
                endGameClicked_ = true;
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage("Are you sure you you want to stop the game?").
                        setPositiveButton("Yes", dialogClickListener)
                        .setNegativeButton("No", dialogClickListener).show();
                break;
            }
            case R.id.b_test_place_cards: {
                Log.d("BoardMainGameActivity", "Test button clicked");
                List<CardData> testCards = new ArrayList<>();
                testCards.add(new CardData(Color.RED, Value.THIRTEEN));
                testCards.add(new CardData(Color.BLUE, Value.THIRTEEN));
                testCards.add(new CardData(Color.GREEN, Value.THIRTEEN));

                testCards.add(new CardData(Color.YELLOW, Value.ONE));
                testCards.add(new CardData(Color.YELLOW, Value.TWO));
                testCards.add(new CardData(Color.JOKER, Value.ONE));
                testCards.add(new CardData(Color.YELLOW, Value.FOUR));

                receiveCardsFromPlayer(testCards, gameData_.playersData_.getCurrentPlayer());
                break;
            }
            default:
                break;
        }
    }

    /**
     * Opens new activity
     *
     * @param winningPlayers
     */
    private void openGameOverActivity(List<String> winningPlayers) {
        Log.d("BoardMainGameActivity", "openGameOverActivity");
        boardTimer_.stop();
        ApplicationData.boardMainGameClass = null;
        Intent intent = new Intent(this, BoardGameOverActivity.class);
        intent.putStringArrayListExtra("winner", (ArrayList<String>) winningPlayers);
        startActivity(intent);
        finish();
    }


    /**
     * Timer in the game.
     * Each player has 2 minutes for each round.
     * If time is up the round ends.
     */
    public class BoardTimer {
        private Timer timer_;
        private Button tv_;
        private boolean paused = false;
        int twoMins = 120000;
        int tenSecs = 10000;
        int oneSec = 1000;
        public long millisInFutureBackup_;

        BoardTimer(Button tv) {
            timer_ = new Timer(twoMins, oneSec);
            tv_ = tv;
        }

        public void click() {
            if (paused == true) {
                unPause();
            } else {
                pause();
            }
        }

        public void pause() {
            Log.d("BoardMainGameActivity", "boardTimer.pause");
            timer_.cancel();
            paused = true;
        }

        public void unPause() {
            Log.d("BoardMainGameActivity", "boardTimer.unPause");
            timer_ = new Timer(millisInFutureBackup_, oneSec);
            timer_.start();
            paused = false;
        }

        public void start() {
            Log.d("BoardMainGameActivity", "boardTimer.start");
            paused = false;
            timer_.start();
        }

        public void stop() {
            Log.d("BoardMainGameActivity", "boardTimer.stop");
            timer_.cancel();
            timer_ = new Timer(twoMins, oneSec);
            millisInFutureBackup_ = twoMins;
        }

        private class Timer extends CountDownTimer {

            /**
             * @param millisInFuture    The number of millis in the future from the call
             *                          to {@link #start()} until the countdown is done and {@link #onFinish()}
             *                          is called.
             * @param countDownInterval The interval along the way to receive
             *                          {@link #onTick(long)} callbacks.
             */
            public Timer(long millisInFuture, long countDownInterval) {
                super(millisInFuture, countDownInterval);
            }

            @Override
            public void onTick(long millisUntilFinished) {
                tv_.setText("" + millisUntilFinished / 1000);
                millisInFutureBackup_ = millisUntilFinished;
            }

            /**
             * If time ends - if play is legal turn ends succesfuly.
             * Otherwise player gets penalty (even if they did not place cards)
             */

            @Override
            public void onFinish() {
                stop();
                gameData_.onEndTurnButtonClick();
                timer_.start();

            }
        }
    }


    /**
     * This class stores the game data.
     */
    class GameData {

        private PlayersData playersData_;
        private Deck deck_;
        private List<String> players_;
        int initialNumOfCardsPerPlayer_ = 14;
        private Card[] cardGridBeforeCurrentTurn_;

        /**
         * Function is called when activity starts.
         *
         * @param players players in game
         */
        private GameData(List<String> players) {
            Log.d("BoardMainGameActivity", "GameData");

            if (players == null) {
                throw new UnsupportedOperationException("Null args");
            }

            int maxPlayer = 4;
            int minPlayers = 2;
            if (players.size() > maxPlayer || players.size() < minPlayers) {
                throw new UnsupportedOperationException("Player number out of range: " + players.size() + "players");
            }

            players_ = new ArrayList<String>();
            for (int i = 0; i < players.size(); i++) {
                players_.add(i, players.get(i));
            }

            deck_ = new Deck();

            // Deal cards:
            List<List<CardData>> initialCards = new ArrayList<List<CardData>>();
            for (String player : players_) {
                initialCards.add(deck_.takeKCards(initialNumOfCardsPerPlayer_));
            }
            playersData_ = new PlayersData(players_, initialCards);

            //Update with empty grid.
            cardGridBeforeCurrentTurn_ = cardsGrid_.asArray();

            //Send initial Cards and inform current player it's there turn

            ApplicationData.communicationClass.BoardToPlayersSendInitialCardsAndInformTurn(playersData_.getRacks(),
                    playersData_.getCurrentPlayer(), ApplicationData.gameId, ApplicationData.gameName);


        }


        /**
         * Function is called when endTurn is clicked.
         * If player started placed cards illegally - turn ends with penalty.
         * If player started placed cards legally - turn ends succesfulley.
         * Update game data - change new player's cards to old cards status.
         */
        private void onEndTurnButtonClick() {

            /******************For testing more than one winner - START***************/
            /*
            Communication.BoardToPlayerGameOver(players_);
            ArrayList<String> p = new ArrayList<String>();
            for(String player: players_) {
                p.add(player);
            }
            openGameOverActivity(p);
            */
            /******************For testing more than one winner - END***************/

            Log.d("BoardMainGameActivity", "GameData.onEndTurnButtonClick");
            if (isPlayLegal()) {
                endTurnWithLegalPlay();
            } else {
                endTurnWithPenalty();
            }
        }

        /**
         * Function is called when deck is clicked.
         * If player started placing cards - turn ends with penalty.
         * If deck is empty - game ends and winner is the one with less hand value
         * Else - turn ends with player drawing a card from the deck.
         * Update game data - change new player's cards to old cards status.
         */
        private void onDeck() {
            Log.d("BoardMainGameActivity", "GameData.onDeck");

            CardData drawnCard;
            try {
                drawnCard = deck_.takeCard();
            } catch (Exception e) {
                endGameWithoutEmptyRack();
                return;
            }
            if (deck_.isEmpty()) {
                deckButton_.setVisibility(View.INVISIBLE);
            }
            playersData_.addCardToCurrentPlayer(drawnCard);
            ApplicationData.communicationClass.BoardToPlayerTurnSuccessfulCardTaken(
                    playersData_.getCurrentPlayer(), drawnCard, ApplicationData.gameId, ApplicationData.gameName);
            cardsGrid_.updateGrid(cardGridBeforeCurrentTurn_);
            updateNextTurnOrEndGame();
        }


        /**
         * Function is called when player sends cards to board.
         * It displays Player's cards in "New Cards" display
         *
         * @param cardsFromPlayer
         */
        private void receiveCardsFromPlayer(List<CardData> cardsFromPlayer) {
            Log.d("BoardMainGameActivity", "GameData.receiveCardsFromPlayer");

            playersData_.addToNewCards(cardsFromPlayer);
        }


        /**
         * Checks if play is legal.
         * Playe is legal if
         * 1. current player placed card
         * 2. terminal is empty
         * 3. The card grid is composed of legal runs and groups only
         *
         * @return true if play is legal. Else, false
         */

        private boolean isPlayLegal() {

            Log.d("BoardMainGameActivity", "GameData.isPlayLegal");

            if (terminal_.getChildCount() != 0) {
                Log.d("GameData.isPlayLegal", "Terminal is not empty");
                return false;
            }
            if (!playersData_.didPlayerPlaceCards()) {
                Log.d("GameData.isPlayLegal", "Player did not place cards");
                return false;
            }

            return cardsGrid_.isLegalBoard();


        }

        /**
         * End turn when play is legal.
         * Notify current player of legal play and next player of their turn
         */
        private void endTurnWithLegalPlay() {
            Log.d("BoardMainGameActivity", "GameData.endTurnSuccessfully");

            //Update “New Cards” to “Old Cards”
            for (Card card : cardsGrid_.getCardsList()) {
                ((BoardCard) card).makeOld();
            }

            //Update current player that turn was ended with cards placed (successfully)
            ApplicationData.communicationClass.BoardToPlayerTurnSuccessful(playersData_.getCurrentPlayer(),
                    playersData_.getNewCards(), ApplicationData.gameId, ApplicationData.gameName);
            playersData_.removeCardsFromCurrentPlayerAfterLegalPlay();
            updateNextTurnOrEndGame();
        }

        /**
         * Called when play was not legal and player gets 3 penalty cards.
         * (The player needs to return their cards to their rack)
         */
        private void endTurnWithPenalty() {
            Log.d("BoardMainGameActivity", "GameData.endTurnWithPenalty");
            List<CardData> penaltyCards;

            int numberOfCardsForPenalty = 3;
            penaltyCards = deck_.takeKOrLessCards(numberOfCardsForPenalty);

            if (deck_.isEmpty()) {
                deckButton_.setVisibility(View.INVISIBLE);
            }
            playersData_.addCardsToCurrentPlayer(penaltyCards);
            ApplicationData.communicationClass.BoardToPlayerTurnUnsuccessful(playersData_.getCurrentPlayer(),
                    penaltyCards, ApplicationData.gameId, ApplicationData.gameName);

            if (penaltyCards.size() != numberOfCardsForPenalty) {
                endGameWithoutEmptyRack();
            } else {
                // Recover previous cardGrid and remove cards from terminal
                terminal_.removeAllViewsInLayout();
                cardsGrid_.updateGrid(cardGridBeforeCurrentTurn_);

                updateNextTurnOrEndGame();
            }
        }


        /**
         * Checks if current player finished their cards.
         * If they did - end game - they are the winner.
         * Else - setup next turn
         */
        private void updateNextTurnOrEndGame() {
            Log.d("BoardMainGameActivity", "GameData.updateNextTurnOrEndGame");

            if (!playersData_.didCurrentPlayerFinish()) {
                updateNextTurn(null);
            } else {
                ArrayList<String> winners = new ArrayList<String>();
                winners.add(playersData_.getCurrentPlayer());
                ApplicationData.communicationClass.BoardToPlayerGameOver(winners, ApplicationData.playersList
                        , ApplicationData.gameId, ApplicationData.gameName);
                boardTimer_.stop();
                openGameOverActivity(winners);

            }
        }

        /**
         * Setup next turn. PlayerName is next to play.
         * If null - the next to play is decided internally.
         *
         * @param playerName update to playerName's turn
         */
        private void updateNextTurn(String playerName) {
            playersData_.updateTurn(playerName);
            ApplicationData.communicationClass.BoardToPlayerInformYourTurn(playersData_.getCurrentPlayer()
                    , ApplicationData.gameId, ApplicationData.gameName);
            cardGridBeforeCurrentTurn_ = cardsGrid_.asArray();
            endTurnButton_.setEnabled(false);
            deckButton_.setEnabled(true);
            displayPlayersTurn();
        }

        /**
         * Called when the game ends with no empty rack.
         * This happens when deck is emptied, game is finished explicitly or
         * a player leaves the game and there are not enough players
         */
        private void endGameWithoutEmptyRack() {
            List<String> winners = playersData_.getPlayersWithMinimalValue();
            ApplicationData.communicationClass.BoardToPlayerGameOver(winners, ApplicationData.playersList
                    , ApplicationData.gameId, ApplicationData.gameName);
            boardTimer_.stop();
            openGameOverActivity(winners);
        }


        /**
         * Function updates game data now that player left the game.
         * If there are not enough players - game ends.
         *
         * @param playerToLeave player that left the game
         */
        private void playerLeavesGame(String playerToLeave) {
            gameData_.deck_.returnPlayerCardsToDeck(playersData_.getRacks().get(playerToLeave));
            playersData_.getRacks().get(playerToLeave).clear();
            String nextPlayer = playersData_.getNextPlayer();
            String currentPlayer = playersData_.getCurrentPlayer();
            if (currentPlayer.equals(playerToLeave)) {
                terminal_.removeAllViewsInLayout();
                cardsGrid_.updateGrid(cardGridBeforeCurrentTurn_);
                if (gameData_.playersData_.getRacks().size() > 2) {
                    playersData_.removePlayer(playerToLeave);
                    updateNextTurn(nextPlayer);
                } else {
                    playersData_.removePlayer(playerToLeave);
                    endGameWithoutEmptyRack();
                }
            } else { //Player thet is leaving is not the current player
                if (gameData_.playersData_.getRacks().size() > 2) {
                    playersData_.removePlayer(playerToLeave);
                    playersData_.setCurrentPlayerIndex(players_.indexOf(currentPlayer)); //index has changed
                    //because player list has changed
                    displayPlayersTurn();
                } else {
                    playersData_.removePlayer(playerToLeave);
                    endGameWithoutEmptyRack();
                }

            }
        }
    }
}

/**
 * Class that holds all they players data
 */
class PlayersData {
    private List<String> players_;
    private Map<String, List<CardData>> playersRacks_;
    private List<CardData> newCards_;
    private int currentPlayerIndex_;

    /**
     * Creates players data.
     * Creates rack and fills them with initial cards of each player.
     *
     * @param players      players in the game
     * @param initialCards initial cards delt to players
     * @throws NullPointerException if one of the args are null.
     */
    public PlayersData(List<String> players, List<List<CardData>> initialCards) {
        Log.d("BoardMainGameActivity", "PlayersData.PlayersData");

        if (players == null || initialCards == null) {
            throw new NullPointerException("PlayerData.PlayerData");
        }
        if (players.size() != initialCards.size()) {
            throw new UnsupportedOperationException
                    ("Number of Players isn't equal to number of card lists sent");
        }

        players_ = players;
        playersRacks_ = new HashMap<String, List<CardData>>();
        newCards_ = new ArrayList<CardData>();
        currentPlayerIndex_ = (new Random()).nextInt(players_.size());
        for (String player : players_) {
            playersRacks_.put(player, new ArrayList<CardData>(initialCards.remove(0)));
        }

    }

    /**
     * @param cardsData
     * @return sum of the value of the list of cardData
     */
    private int getHandValue(List<CardData> cardsData) {
        if (cardsData == null) {
            throw new NullPointerException("PlayerData.getHandValue(cards)");
        }
        int sum = 0;
        for (CardData cardData : cardsData) {
            sum += cardData.getNumericValue();
        }
        return sum;
    }

    /**
     * @param player
     * @return sum of the value of the player's rack
     */
    public int getHandValue(String player) {
        if (player == null) {
            throw new NullPointerException("PlayerData.getHandValue(player)");
        }
        return getHandValue(playersRacks_.get(player));
    }

    /**
     * @return list of all the players with minimal hands
     */
    public List<String> getPlayersWithMinimalValue() {
        int min = getHandValue(players_.get(0));
        for (String player : players_) {
            int handValue = getHandValue(player);
            if (handValue < min) {
                min = handValue;
            }
        }
        List<String> winningPlayers = new ArrayList<>();
        for (String player : players_) {
            int handValue = getHandValue(player);
            if (handValue == min) {
                winningPlayers.add(player);
            }
        }
        return winningPlayers;
    }

    /**
     * Adds cards to current player
     *
     * @param cardsData cards to be added
     */
    public void addCardsToCurrentPlayer(List<CardData> cardsData) {
        if (cardsData == null) {
            Log.d("BoardMainGameActivity", "addCardsToCurrentPlayer: cardsData parameter is null");
        }
        playersRacks_.get(getCurrentPlayer()).addAll(cardsData);

    }

    /**
     * Adds card to current player
     *
     * @param cardData card to be added
     */
    public void addCardToCurrentPlayer(CardData cardData) {
        if (cardData == null) {
            Log.d("BoardMainGameActivity", "addCardToCurrentPlayer: cardData parameter is null");
        }
        playersRacks_.get(getCurrentPlayer()).add(cardData);
    }

    /**
     * Updated Player data with the cards the current player placed on board.
     * These are called "new cards"
     *
     * @param cardsData new cards that were placed
     */
    public void addToNewCards(List<CardData> cardsData) {
        if (cardsData == null) {
            Log.d("BoardMainGameActivity", "addToNewCards: cardData parameter is null");
        }
        newCards_.addAll(cardsData);
    }

    /**
     * When turn is succesfull (with placed cards), this function whould be called
     * to update the current players rack (remove new cards
     * (currently placed cards) from rack
     */
    public void removeCardsFromCurrentPlayerAfterLegalPlay() {
        playersRacks_.get(getCurrentPlayer()).removeAll(newCards_);
    }

    /**
     * @return true if current players rack is empty. Else, false.
     */
    public Boolean didCurrentPlayerFinish() {
        int numberOfCardsCurrentPlayerHolds = playersRacks_.get(getCurrentPlayer()).size();
        return (numberOfCardsCurrentPlayerHolds == 0);
    }

    /**
     * @return True if current player placed cards. Else - false
     */
    public Boolean didPlayerPlaceCards() {
        return (newCards_.size() != 0);
    }

    /**
     * Updates turn to playerName's turn.
     * If playerName == null, update in the order that was initially set.
     *
     * @param playerName
     */
    public void updateTurn(String playerName) {
        if (playerName == null) {
            currentPlayerIndex_ = (currentPlayerIndex_ + 1) % (players_.size());
        } else {
            currentPlayerIndex_ = players_.indexOf(playerName);
        }
        newCards_.clear();
    }

    /**
     * @return Current playing player
     */
    public String getCurrentPlayer() {
        return players_.get(currentPlayerIndex_);
    }

    /**
     * Sets current player index (Can be used it this changes because of another player leaving)
     *
     * @param index new index
     */
    public void setCurrentPlayerIndex(int index) {
        currentPlayerIndex_ = index;
    }

    /**
     * @return player that is next to play
     */
    public String getNextPlayer() {
        return players_.get((currentPlayerIndex_ + 1) % (players_.size()));
    }

    /**
     * @return players racks
     */
    public Map<String, List<CardData>> getRacks() {
        return playersRacks_;
    }

    /**
     * @return currently placed cards (new cards)
     */
    public List<CardData> getNewCards() {
        return newCards_;
    }

    /**
     * Removes player from player data and updates data accordingly.
     *
     * @param playerToRemove
     */
    public void removePlayer(String playerToRemove) {
        if (!playersRacks_.get(playerToRemove).isEmpty()) {
            throw new UnsupportedOperationException("trying to remove player that has cards");
        } else {
            playersRacks_.remove(playerToRemove);
            players_.remove(playerToRemove);
            ApplicationData.playersList.remove(playerToRemove);
        }
    }

}
