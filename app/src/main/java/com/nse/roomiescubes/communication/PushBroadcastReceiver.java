package com.nse.roomiescubes.communication;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.nse.roomiescubes.ApplicationData;
import com.nse.roomiescubes.components.card.CardData;
import com.parse.ParsePushBroadcastReceiver;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class PushBroadcastReceiver extends ParsePushBroadcastReceiver {

    /**
     * <b>onInvitationReceive</b> is called when the a push notification of type invitation is received
     *
     * @param pushData The JSON received in the push notification
     */
    private void onInvitationReceive(JSONObject pushData) {
        try {
            Log.d("receiver", "type is invitation");
            String gameName = pushData.getString("gameName");
            String gameId = pushData.getString("gameId");
            Log.d("receiver", "name is:" + gameName);
            if (ApplicationData.invitationClass != null) {
                ApplicationData.invitationClass.onPushInvitation(gameId, gameName);
            } else {
                Log.d("receiver", "onInvitationReceive: ParseApplication.invitationClass is null ");
            }
        } catch (JSONException var7) {
            Log.d("com.parse", "Unexpected JSONException when receiving push data: ");
        }
    }

    /**
     * <b>onJoinGameReceive</b> is called when the a push notification of type joinGame is received
     *
     * @param playerName The name of the player that joined the game as received in the
     *                   push notification's JSON data in the field "playerName"
     */
    private void onJoinGameReceive(String playerName) {
        Log.d("receiver", "player name is:" + playerName);
        if (ApplicationData.boardStartGameClass != null) {
            ApplicationData.boardStartGameClass.onPlayerJoinsGame(playerName);
        } else {
            Log.d("receiver", "onJoinGameReceive: ParseApplication.boardStartGameClass is null ");
        }

    }

    /**
     * <b>onInitialCards</b> is called when the a push notification of type initialCards is received
     *
     * @param initialCards The JSON that was received in the push notification with the initial card's data
     * @param playerName   The name of the player that received the cards as received in the
     *                     push notification's JSON data in the field "playerName"
     * @param gameName     The name of the player that received the cards as received in the
     *                     push notification's JSON data in the field "playerName"
     */
    private void onInitialCards(JSONObject initialCards, String playerName, String gameName) {
        if (ApplicationData.invitationClass != null) {
            ApplicationData.invitationClass.onPushGameStarts(initialCards);
        } else {
            Log.d("receiver:onInitialCards", "main game class is null");
        }
    }


    /**
     * <b>onGameOver</b> is called when the a push notification of type gameOver is received
     *
     * @param winnersList The JSON that was received in the push notification that contains the
     *                    list of the winning players
     */
    private void onGameOver(JSONObject winnersList) {
        List<String> winners = new ArrayList<String>();
        try {
            if (ApplicationData.playerMainGameClass != null) {
                JSONArray winnersArray = winnersList.optJSONArray("winner");
                if (winnersArray != null) { //There is more that one winner
                    for (int i = 0; i < winnersArray.length(); i++) {
                        winners.add(winnersArray.getString(i));
                    }
                    ApplicationData.playerMainGameClass.finishGame(winners);
                } else {
                    winners.add(winnersList.getString("winner"));
                    ApplicationData.playerMainGameClass.finishGame(winners);
                }
            } else {
                Log.d("receiver: onGameOver ", "player main game activity is null");
            }
        } catch (JSONException var7) {
        }
    }

    /**
     * <b>onDeclineGame</b> is called when the a push notification of type "declineGame" is received
     *
     * @param playerName The name of the player that declined the game  as received in the
     *                   push notification's JSON data in the field "playerName"
     */
    private void onDeclineGame(String playerName) {
        if (ApplicationData.boardStartGameClass != null) {
            ApplicationData.boardStartGameClass.onPlayerDeclinesGame(playerName);
        } else {
            Log.d("receiver:onDeclineGame", "board start game activity is null");
        }
    }

    /**
     * <b>onCancelInvitation</b> is called when the a push notification of type "cancelInvitation" is received
     *
     * @param gameName The name of the game that canceled the invitation
     * @param gameId   The Id of the game that canceled the invitation
     */
    private void onCancelInvitation(String gameName, String gameId) {
        if (ApplicationData.invitationClass != null) {
            ApplicationData.invitationClass.onGameCanceledInvitation(gameName, gameId);
        } else {
            Log.d("receiver:", "onCancelInvitation: player wait for invitation activity is null");
        }
    }

    /**
     * <b>onPlayerLeavesGame</b> is called when the a push notification of type "leavesGame" is received
     *
     * @param playerName The name of the player tahe leaves the game
     */
    private void onPlayerLeavesGame(String playerName) {
        if (ApplicationData.boardMainGameClass != null) {
            ApplicationData.boardMainGameClass.playerLeavesGame(playerName);
        } else if (ApplicationData.boardStartGameClass != null) {
            ApplicationData.boardStartGameClass.onPlayerLeavesGame(playerName);
        } else {
            Log.d("receiver", "player left game and relevant activities nul");
        }

    }

    /**
     * <b>onStartPlayerTurn</b> is called when the a push notification of type "startTurn" is received
     */
    private void onStartPlayerTurn() {
        if (ApplicationData.playerMainGameClass != null) {
            ApplicationData.playerMainGameClass.startPlayerTurn();
        } else {
            Log.d("receiver", "onStartPlayerTurn - player main game activity is null");
        }
    }


    /**
     * <b>onEndTurnSuccessfully</b> is called when the a push notification of type
     * "turnSuccessful" is received
     *
     * @param cardsPlaced The cards the were placed on the board on this turn
     */
    private void onEndTurnSuccessfully(List<CardData> cardsPlaced) {
        if (ApplicationData.playerMainGameClass != null) {
            ApplicationData.playerMainGameClass.endTurnSuccessfully(cardsPlaced);
        } else {
            Log.d("receiver", "onEndTurnSuccessfully - player main game activity is null");
        }
    }


    /**
     * <b>onEndTurnSuccessfullyWithCard</b> is called when the a push notification of type
     * "turnSuccessfulWithCard" is received
     *
     * @param deckCard The card that the player received from the deck
     */
    private void onEndTurnSuccessfullyWithCard(CardData deckCard) {
        if (ApplicationData.playerMainGameClass != null) {
            ApplicationData.playerMainGameClass.endTurnSuccessfully(deckCard);
        } else {
            Log.d("receiver", "onEndTurnSuccessfullyWithCard - player main game activity is null");
        }
    }


    /**
     * <b>onEndTurnWithPenalty</b> is called when the a push notification of type
     * "penaltyCards" is received
     *
     * @param cardsList The list of cards that the player received from the board as penalty
     */
    private void onEndTurnWithPenalty(List<CardData> cardsList) {
        if (ApplicationData.playerMainGameClass != null) {
            ApplicationData.playerMainGameClass.endTurnWithPenalty(cardsList);
        } else {
            Log.d("receiver", "onEndTurnWithPenalty - player main game activity is null");
        }

    }

    /**
     * <b>onGameStoppedUnexpectedly</b> is called when the a push notification of type
     * "GameStoppedUnexpectedly" is received
     *
     * @param gameName The name if the game that was stopped
     * @param gameId   The Id if the game that was stopped
     */
    private void onGameStoppedUnexpectedly(String gameName, String gameId) {
        if (ApplicationData.playerMainGameClass != null) {
            if (verifySenderGame(gameId) == false) {
                return;
            } else {
                ApplicationData.playerMainGameClass.gameStoppedUnexpectedly();
            }
        } else if (ApplicationData.invitationClass != null) {
            ApplicationData.invitationClass.onGameCanceledInvitation(gameName, gameId);
        } else {
            Log.d("receiver", "onEndTurnWithPenalty - player main game activity is null");
        }

    }


    /**
     * <b>onReceiveCardsFromPlayer</b> is called when the a push notification of type
     * "cards" is received
     *
     * @param cardsList  The cards the are received from the player
     * @param playerName The name of the player that sent the cards
     */
    private void onReceiveCardsFromPlayer(List<CardData> cardsList, String playerName) {
        if (ApplicationData.boardMainGameClass != null) {
            ApplicationData.boardMainGameClass.receiveCardsFromPlayer(cardsList, playerName);
        } else {
            Log.d("receiver", "onReceiveCardsFromPlayer - board main game activity is null");
        }
    }


    /**
     * <b>verifySenderGame</b> is called if the role type of the receiving  user is "player"
     * in order to verify that the board sending the message is the board that this player
     * is belonged to
     *
     * @param gameId The Id of the player that sent the message
     */
    private boolean verifySenderGame(String gameId) {
        if (ApplicationData.gameId != null) {
            Log.d("receiver", "parse application gameId is not null");
            if (ApplicationData.gameId.equals(gameId)) {
                Log.d("receiver", "parse application gameId is equal to sender id");
                return true;
            } else {
                Log.d("receiver", "parse application gameId is different than sender id");
                return false;
            }
        } else {
            Log.d("receiver", "parse application gameId is null");
            return true;
        }
    }

    @Override
    public void onPushReceive(Context context, Intent intent) {

        JSONObject pushData = null;
        String type = null;
        String gameId;
        try {
            Log.d("json", "Get Json");
            pushData = new JSONObject(intent.getStringExtra("com.parse.Data"));
            Log.d("json", "Get type now");
            type = pushData.getString("type");

            Log.d("json", "Got type!!");

            /*****************We are a player *************************/
            if (ApplicationData.playerType == null) {
                Log.d("receiver", "player type is null");
                return;
            }
            if (ApplicationData.playerType.equals("player")) {
                gameId = pushData.getString("gameId");
                Log.d("receiver", "I am a player");
                if (type.equals("invitation")) {
                    onInvitationReceive(pushData);
                } else if (type.equals("initialCardsAndInformTurn")) {
                    if (verifySenderGame(gameId) != true) {
                        return;
                    }
                    onInitialCards(pushData, pushData.getString("playerName"), pushData.getString("gameName"));
                } else if (type.equals("beginTurn")) {
                    if (verifySenderGame(gameId) != true) {
                        return;
                    }
                    onStartPlayerTurn();
                } else if (type.equals("turnSuccessful")) {
                    if (verifySenderGame(gameId) != true) {
                        return;
                    }
                    onEndTurnSuccessfully(CardData.getCardsDataListFromJSON(pushData));
                } else if (type.equals("turnSuccessfulWithCard")) {
                    if (verifySenderGame(gameId) != true) {
                        return;
                    }
                    onEndTurnSuccessfullyWithCard(CardData.getCardDataFromJSON(pushData));
                } else if (type.equals("penaltyCards")) {
                    if (verifySenderGame(gameId) != true) {
                        return;
                    }
                    onEndTurnWithPenalty(CardData.getCardsDataListFromJSON(pushData));
                } else if (type.equals("gameOver")) {
                    if (verifySenderGame(gameId) != true) {
                        return;
                    }
                    onGameOver(pushData);
                } else if (type.equals("GameStoppedUnexpectedly")) {
                    onGameStoppedUnexpectedly(pushData.getString("gameName"), gameId);
                } else if (type.equals("cancelInvitation")) {
                    onCancelInvitation(pushData.getString("gameName"), gameId);
                }
            }

            /*****************We are a board *************************/
            else {
                Log.d("receiver", "I am a board");
                if (type.equals("joinGame")) {
                    onJoinGameReceive(pushData.getString("playerName"));
                } else if (type.equals("cards")) {
                    /************Need to call Ella's method***************/
                    onReceiveCardsFromPlayer(CardData.getCardsDataListFromJSON(pushData),
                            (pushData.getString("playerName")));
                } else if (type.equals("leaveGame")) {
                    onPlayerLeavesGame(pushData.getString("playerName"));

                } else if (type.equals("declineGame")) {
                    onDeclineGame(pushData.getString("playerName"));
                }

            }
        } catch (JSONException var7) {
            Log.d("com.parse", "Unexpected JSONException when receiving push data: ");
        }


    }


}
