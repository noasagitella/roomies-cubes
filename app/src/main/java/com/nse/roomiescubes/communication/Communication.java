package com.nse.roomiescubes.communication;


import android.util.Log;

import com.nse.roomiescubes.components.card.CardData;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParsePush;
import com.parse.ParseQuery;
import com.parse.SendCallback;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;
import java.util.Map;

/**
 * Created by t-sashap on 12-06-15.
 */
public class Communication implements ICommunication{

    /**
     * **************************************BOARD TO PLAYER - START******************************
     */


    /**
     * <b>getParseQueryByPlayer</b> creates and and returns a Parse query for a user with
     * the username: playerName that is already belonged to a game
     *
     * @param playerName The player's name to create the query for
     * @return The query for the Parse user with username: playerName
     */
    private ParseQuery getParseQueryByPlayer(String playerName, String gameName) {
        ParseQuery pushQuery = ParseInstallation.getQuery();
        pushQuery.whereEqualTo("roleType", "player");
        pushQuery.whereEqualTo("gameName", gameName);
        pushQuery.whereEqualTo("userName", playerName);
        return pushQuery;
    }

    /**
     * <b>getParseQueryByPlayerNoGame</b> creates and and returns a Parse query for a user with
     * the username: playerName that is not belonged to a game
     *
     * @param playerName The player's name to create the query for
     * @return The query for the Parse user with username: playerName
     */
    private ParseQuery getParseQueryByPlayerNoGame(String playerName) {
        ParseQuery pushQuery = ParseInstallation.getQuery();
        pushQuery.whereEqualTo("roleType", "player");
        pushQuery.whereEqualTo("userName", playerName);
        return pushQuery;
    }

    /**
     * <b>getParseQueryForBoard</b> creates and and returns a Parse query for am Installation
     * that belongs to the game with the name: gameName and that had the Id: gameId
     *
     * @param gameId   The Installation Id to create the query for
     * @param gameName The Installation game's name to create the query for
     * @return The query for the Parse user with username: playerName
     */
    private ParseQuery getParseQueryForBoard(String gameId, String gameName) {
        ParseQuery pushQuery = ParseInstallation.getQuery();
        pushQuery.whereEqualTo("roleType", "board");
        pushQuery.whereEqualTo("installationId", gameId);
        pushQuery.whereEqualTo("gameName", gameName);
        return pushQuery;
    }

    /**
     * <b>sendPushByType</b> sends a push notification with type: type
     *
     * @param query the query that represents the destination of this push notification
     * @param type  The type of this push notification
     * @param data  The JSON object to be sent in tis push notification
     * @return The query for the Parse user with username: playerName
     */
    private void sendPushByType(ParseQuery query, String type, JSONObject data) {
        ParsePush push = new ParsePush();
        push.setQuery(query); // Set our Installation query
        push.setData(data);
        Log.d(type, "send JSON now");
        push.sendInBackground(new SendCallback() {
            public void done(ParseException e) {
                if (e == null) {
                    Log.d("Communication", "The push campaign has been created.");
                } else {
                    Log.d("Communication", "Error sending push:" + e.getMessage());
                }
            }
        });
    }

    /**
     * <b>BoardToPlayerInvitePlayer</b> is called when creating a new game and
     * a player is invited to the game.
     *
     * @param player   The player name that is being invited to the new game.
     * @param gameId   the Id of the game that invites the player
     * @param gameName the name of the game the invites the player
     */

    public void BoardToPlayerInvitePlayer(String player, String gameId, String gameName) {
        Log.d("Communication", "BoardToPlayerInvitePlayer");
        ParseQuery pushQuery = ParseInstallation.getQuery();
        pushQuery.whereEqualTo("roleType", "player");
        pushQuery.whereEqualTo("userName", player);
        JSONObject data = new JSONObject();

        try {
            data.put("type", "invitation");
            data.put("gameName", gameName);
            data.put("gameId", gameId);
            sendPushByType(pushQuery, "invitation", data);
        } catch (JSONException e1) {
            Log.d("COM: sentInitialCards", "JSONException" + e1.getMessage());
        }
    }


    /**
     * <b>BoardToSinglePlayerSendInitialCards</b> is called when a game is initiated and
     * initial cards are sent to <b>a player</b>.
     *
     * @param player       The player that has to receive the cards.
     * @param cardsData    The initial cards sent.
     * @param isPlayerTurn true if the player is the first to play (this is his turn), false otherwise
     * @param gameId       the Id of the game sending this the cards
     * @param gameName     the name of the game sending this the cards
     */
    private void BoardToSinglePlayerSendInitialCards(String player,
                                                            List<CardData> cardsData,
                                                            Boolean isPlayerTurn,
                                                            String gameId,
                                                            String gameName) {
        Log.d("Communication", "BoardToSinglePlayerSendInitialCards");
        ParseQuery pushQuery = getParseQueryByPlayer(player, gameName);
        Log.d("COM: sentInitialCards", "Board is sending initial cards to " + player);
        Log.d("COM: sentInitialCards", "is player " + player + " starting?" + isPlayerTurn);

        try {
            JSONObject data = CardData.getJSONArrayFromCardsDataList(cardsData);
            data.put("type", "initialCardsAndInformTurn");
            data.put("isPlayerTurn", isPlayerTurn);
            data.put("playerName", player);
            data.put("gameId", gameId);
            data.put("gameName", gameName);

            sendPushByType(pushQuery, "initialCards", data);
            Log.d("COM: sentInitialCards", "PUSHED");
        } catch (JSONException e1) {
            Log.d("COM: sentInitialCards", "JSONException" + e1.getMessage());
        }
    }


    /**
     * <b>BoardToPlayersSendInitialCardsAndInformTurn</b> is called when a game is initiated and
     * initial cards are sent to <b>all players</b>.
     *
     * @param playersCards   Map of Players to cards List - each player has his initial cards
     * @param playerStarting The name of the first player to play in the game
     * @param gameId         the Id of the game sending this the cards
     * @param gameName       the name of the game sending this the cards
     */
    public void BoardToPlayersSendInitialCardsAndInformTurn(Map<String,
            List<CardData>> playersCards, String playerStarting, String gameId, String gameName) {
        Log.d("Communication", "BoardToPlayerSendInitialCards");

        for (String key : playersCards.keySet()) {
            BoardToSinglePlayerSendInitialCards(key, playersCards.get(key),
                    key.equals(playerStarting), gameId, gameName);
        }

    }

    /**
     * <b>BoardToPlayerInformYourTurn</b> is called when the board informs a player that
     * it is that player's turn to play.
     *
     * @param player The player that will play next.
     * @param gameId The Id of the game
     */

    public void BoardToPlayerInformYourTurn(String player, String gameId, String gameName) {
        Log.d("Communication", "BoardToPlayerInformYourTurn");
        ParseQuery pushQuery = getParseQueryByPlayer(player, gameName);
        Log.d("COM: informYourTurn", "Board is sending initial cards to " + player);
        JSONObject data;
        try

        {
            data = new JSONObject();
            data.put("type", "beginTurn");
            data.put("gameId", gameId);
            sendPushByType(pushQuery, "beginTurn", data);
            Log.d("COM: informYourTurn", "PUSHED");
        } catch (JSONException e1) {
            Log.d("COM: informYourTurn", "JSONException" + e1.getMessage());
        }
    }


    /**
     * <b>BoardToPlayerTurnSuccessful</b> is called when the player has moved cards to the board,
     * and placed them successfully on the board and pressed on "End Turn" button.
     *
     * @param player      The player that finished a successful turn.
     * @param cardsPlaced A list of cards that were placed on the board
     */
    public void BoardToPlayerTurnSuccessful(String player, List<CardData> cardsPlaced,
                                                   String gameId, String gameName) {
        Log.d("Communication", "BoardToPlayerTurnSuccessful");
        ParseQuery pushQuery = getParseQueryByPlayer(player, gameName);
        Log.d("COM: turnSuccessful", "Board is sending turn successful " + player);
        JSONObject data;
        try {
            data = CardData.getJSONArrayFromCardsDataList(cardsPlaced);
            data.put("type", "turnSuccessful");
            data.put("gameId", gameId);
            sendPushByType(pushQuery, "turnSuccessful", data);
            Log.d("COM: turnSuccessful", "PUSHED");
        } catch (JSONException e1) {
            Log.d("COM: turnSuccessful", "JSONException" + e1.getMessage());
        }
    }


    /**
     * <b>BoardToPlayerTurnSuccessfulCardTaken</b> is called when the player has taken a card
     * from the deck without moving cards to the board.
     *
     * @param player    The player that finished an successful turn.
     * @param takenCard The card from the deck.
     * @param gameId    The Id of the game
     */
    public void BoardToPlayerTurnSuccessfulCardTaken(String player, CardData takenCard,
                                                            String gameId, String gameName) {
        Log.d("Communication", "BoardToPlayerTurnSuccessfulCardTaken");

        ParseQuery pushQuery = getParseQueryByPlayer(player, gameName);
        Log.d("COM: turnSuccessfulCard", "Board is sending turn successful " + player);

        try {
            JSONObject data = CardData.getJSONFromCardData(takenCard);
            data.put("type", "turnSuccessfulWithCard");
            data.put("gameId", gameId);
            sendPushByType(pushQuery, "turnSuccessfulWithCard", data);
            Log.d("COM: turnSuccessfulCard", "PUSHED");
        } catch (JSONException e1) {
            Log.d("COM: turnSuccessfulCard", "JSONException" + e1.getMessage());
        }
    }


//TODO (from Ella) this needs to be sent rigt before BoardToPlayerTurnSuccessful and not instead of - This is because sometimes the deck will be empty

    /**
     * <b>BoardToPlayerTurnUnsuccessful</b> is called when the player has moved cards
     * to the board and pressed on "End Turn" button without a successful placing on the board.
     *
     * @param player       The player that finished a unsuccessful turn.
     * @param penaltyCards Three penalty cards from the deck.
     * @param gameId       The Id of the game
     */
    public void BoardToPlayerTurnUnsuccessful(String player, List<CardData> penaltyCards,
                                                     String gameId, String gameName) {
        Log.d("Communication", "BoardToPlayerTurnUnsuccessful");

        ParseQuery pushQuery = getParseQueryByPlayer(player, gameName);
        try {

            JSONObject data = CardData.getJSONArrayFromCardsDataList(penaltyCards);
            data.put("type", "penaltyCards");
            data.put("gameId", gameId);
            sendPushByType(pushQuery, "penaltyCards", data);
            Log.d("COM: penaltyCards", "PUSHED");
        } catch (JSONException e1) {
            Log.d("COM: penaltyCards", "JSONException" + e1.getMessage());
        }
    }


    /**
     * <b>BoardToPlayerGameOver</b> is called when the current player finished a successful move
     * and finished all of their cards - this player has won the game or when the board
     * wants to finish the game early (by clicking on end game button)
     * A message with the identity of the winner is sent to all the players.
     *
     * @param winners The players that have won the game.
     * @param players The players that are playing the game
     * @param gameId  The Id of the game
     */
    public void BoardToPlayerGameOver(List<String> winners, List<String> players,
                                             String gameId, String gameName) {
        Log.d("Communication", "BoardToPlayerGameOver");
        for (String player : players) {
            ParseQuery pushQuery = getParseQueryByPlayer(player, gameName);
            Log.d("COM:GameOver", "creating push");
            JSONObject data;
            try {
                data = new JSONObject();
                data.put("type", "gameOver");
                data.put("gameId", gameId);
                for (String winner : winners) {
                    data.accumulate("winner", winner);
                }
                sendPushByType(pushQuery, "GameOver", data);
                Log.d("COM:GameOver", "PUSHED");
            } catch (JSONException e1) {
                Log.d("COM:GameOver", "JSONException" + e1.getMessage());
            }
        }

    }

    /**
     * <b>BoardToPlayerGameStoppedUnexpectedly</b> is called the board leaves the game unexpectedly
     *
     * @param gameId         the Id of the game that was stopped
     * @param gameName       the name of the game that was stopped
     * @param invitedPlayers The name of the players that belong to this game
     */
    public void BoardToPlayerGameStoppedUnexpectedly(String gameId, String gameName,
                                                            List<String> invitedPlayers) {
        Log.d("Communication", "BoardToPlayerGameStoppedUnexpectedly");
        for (String player : invitedPlayers) {
            Log.d("COM:StoppedUnexpectedly", "creating push");
            JSONObject data;
            ParseQuery pushQuery = getParseQueryByPlayerNoGame(player);
            try {
                data = new JSONObject();
                data.put("type", "GameStoppedUnexpectedly");
                data.put("gameName", gameName);
                data.put("gameId", gameId);
                sendPushByType(pushQuery, "GameStoppedUnexpectedly", data);
                Log.d("COM:StoppedUnexpectedly", "PUSHED");
            } catch (JSONException e1) {
                Log.d("COM:StoppedUnexpectedly", "JSONException" + e1.getMessage());
            }
        }

    }

    /**
     * <b>BoardToPlayerCancelInvitation</b> is called the board cancels an invitation sent to
     * a player
     *
     * @param gameId     the Id of the game canceling the invitation
     * @param gameName   the name of the game canceling the invitation
     * @param playerName The name of hte player that his invitation is canceled
     */
    public void BoardToPlayerCancelInvitation(String gameId, String gameName,
                                                     String playerName) {
        Log.d("Communication", "BoardToPlayerCancelInvitation");
        ParseQuery pushQuery = ParseInstallation.getQuery();
        pushQuery.whereEqualTo("roleType", "player");
        pushQuery.whereEqualTo("userName", playerName);
        Log.d("COM:cancelInvitation", "creating push");
        JSONObject data;
        try {
            data = new JSONObject();
            data.put("type", "cancelInvitation");
            data.put("gameName", gameName);
            data.put("gameId", gameId);
            sendPushByType(pushQuery, "cancelInvitation", data);
            Log.d("COM:cancelInvitation", "PUSHED");
        } catch (JSONException e1) {
            Log.d("COM:cancelInvitation", "JSONException" + e1.getMessage());
        }
    }


/*****************************************BOARD TO PLAYER - END*******************************/

/*****************************************PLAYER TO BOARD - START*******************************/

    /**
     * <b>PlayerToBoardNewCards</b> is called when the current player sends cards to
     * the board.
     *
     * @param cardsData  The cards sent to the board.
     * @param gameId     The Id of the game canceling the invitation
     * @param gameName   The name of the game canceling the invitation
     * @param playerName The name of the player sending the cards
     */
    public void PlayerToBoardNewCards(List<CardData> cardsData, String gameId, String gameName,
                                             String playerName) {
        Log.d("Communication", "PlayerToBoardNewCards");
        ParseQuery pushQuery = getParseQueryForBoard(gameId, gameName);
        try {

            JSONObject data = CardData.getJSONArrayFromCardsDataList(cardsData);
            data.put("type", "cards");
            data.put("playerName", playerName);
            Log.d("playerName", playerName);
            sendPushByType(pushQuery, "cards", data);
            Log.d("COM: playerToBoardcards", "PUSHED");
        } catch (JSONException e1) {
            Log.d("COM: playerToBoardcards", "JSONException" + e1.getMessage());
        }

    }

    /**
     * <b>PlayerToBoardJoinGame</b> is called when the player accepts an invitation sent
     * from the board and he is joining the game
     *
     * @param gameId     The Id of the game that it's invitation is accepted
     * @param gameName   The name of the game that it's invitation is accepted
     * @param playerName The name of the player that accepts the invitation
     */
    public void PlayerToBoardJoinGame(String gameId, String gameName, String playerName) {
        ParseQuery pushQuery = getParseQueryForBoard(gameId, gameName);
        Log.d("COM: JoinGame", "Player push data to join game");
        try {
            JSONObject data = new JSONObject();
            data.put("type", "joinGame");
            data.put("playerName", playerName);
            sendPushByType(pushQuery, "joinGame", data);
            Log.d("COM: JoinGame", "PUSHED");
        } catch (JSONException e1) {
            Log.d("COM: JoinGame", "JSONException" + e1.getMessage());
        }
    }

    /**
     * <b>PlayerToBoardDeclineGame</b> is called when the player declined an invitation sent
     * from the board and he is joining the game
     *
     * @param gameId     The Id of the game that it's invitation is declined
     * @param gameName   The name of the game that it's invitation is declined
     * @param playerName The name of the player that declines the invitation
     */
    public void PlayerToBoardDeclineGame(String gameId, String gameName, String playerName) {
        ParseQuery pushQuery = getParseQueryForBoard(gameId, gameName);
        Log.d("COM: DeclineGame", "Player push data to decline game");
        try {
            JSONObject data = new JSONObject();
            data.put("type", "declineGame");
            data.put("playerName", playerName);
            sendPushByType(pushQuery, "declineGame", data);
            Log.d("COM: leaveGame", "PUSHED");
        } catch (JSONException e1) {
            Log.d("COM: DeclineGame", "JSONException" + e1.getMessage());
        }
    }

    /**
     * <b>PlayerToBoardLeaveGame</b> is called when the player leaves the game after he
     * was invited by a board
     *
     * @param gameId     The Id of the game that invited the player
     * @param gameName   The name of the game that invited the player
     * @param playerName The name of the player that leaves the game
     */
    public void PlayerToBoardLeaveGame(String gameId, String gameName, String playerName) {
        ParseQuery pushQuery = getParseQueryForBoard(gameId, gameName);

        Log.d("COM: JoinGame", "Player push data to join game");
        try {
            JSONObject data = new JSONObject();
            data.put("type", "leaveGame");
            data.put("playerName", playerName);
            sendPushByType(pushQuery, "leaveGame", data);
            Log.d("COM: leaveGame", "PUSHED");
        } catch (JSONException e1) {
            Log.d("COM: leaveGame", "JSONException" + e1.getMessage());
        }
    }

/*****************************************PLAYER TO BOARD - END*******************************/
}

