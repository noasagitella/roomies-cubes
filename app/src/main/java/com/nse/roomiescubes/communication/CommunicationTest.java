package com.nse.roomiescubes.communication;

import android.util.Log;

import com.nse.roomiescubes.components.card.CardData;

import java.util.List;
import java.util.Map;

/**
 * Created by Sagit on 25/08/2015.
 */
public class CommunicationTest implements ICommunication {

    /**
     * **************************************BOARD TO PLAYER - START******************************
     */

    /**
     * <b>BoardToPlayerInvitePlayer</b> is called when creating a new game and
     * a player is invited to the game.
     *
     * @param player   The player name that is being invited to the new game.
     * @param gameId   the Id of the game that invites the player
     * @param gameName the name of the game the invites the player
     */
    public void BoardToPlayerInvitePlayer(String player, String gameId, String gameName) {
        Log.d("CommunicationTest", "BoardToPlayerInvitePlayer");
    }

    /**
     * <b>BoardToPlayersSendInitialCardsAndInformTurn</b> is called when a game is initiated and
     * initial cards are sent to <b>all players</b>.
     *
     * @param playersCards   Map of Players to cards List - each player has his initial cards
     * @param playerStarting The name of the first player to play in the game
     * @param gameId         the Id of the game sending this the cards
     * @param gameName       the name of the game sending this the cards
     */
    public void BoardToPlayersSendInitialCardsAndInformTurn(Map<String,
            List<CardData>> playersCards, String playerStarting, String gameId, String gameName) {
        Log.d("CommunicationTest", "BoardToPlayersSendInitialCardsAndInformTurn");
    }

    /**
     * <b>BoardToPlayerInformYourTurn</b> is called when the board informs a player that
     * it is that player's turn to play.
     *
     * @param player The player that will play next.
     * @param gameId The Id of the game
     */

    public void BoardToPlayerInformYourTurn(String player, String gameId, String gameName) {
        Log.d("CommunicationTest", "BoardToPlayerInformYourTurn");
    }


    /**
     * <b>BoardToPlayerTurnSuccessful</b> is called when the player has moved cards to the board,
     * and placed them successfully on the board and pressed on "End Turn" button.
     *
     * @param player      The player that finished a successful turn.
     * @param cardsPlaced A list of cards that were placed on the board
     */
    public void BoardToPlayerTurnSuccessful(String player, List<CardData> cardsPlaced,
                                            String gameId, String gameName) {
        Log.d("CommunicationTest", "BoardToPlayerTurnSuccessful");
    }


    /**
     * <b>BoardToPlayerTurnSuccessfulCardTaken</b> is called when the player has taken a card
     * from the deck without moving cards to the board.
     *
     * @param player    The player that finished an successful turn.
     * @param takenCard The card from the deck.
     * @param gameId    The Id of the game
     */
    public void BoardToPlayerTurnSuccessfulCardTaken(String player, CardData takenCard,
                                                     String gameId, String gameName) {
        Log.d("CommunicationTest", "BoardToPlayerTurnSuccessfulCardTaken");
    }


//TODO (from Ella) this needs to be sent rigt before BoardToPlayerTurnSuccessful and not instead of - This is because sometimes the deck will be empty

    /**
     * <b>BoardToPlayerTurnUnsuccessful</b> is called when the player has moved cards
     * to the board and pressed on "End Turn" button without a successful placing on the board.
     *
     * @param player       The player that finished a unsuccessful turn.
     * @param penaltyCards Three penalty cards from the deck.
     * @param gameId       The Id of the game
     */
    public void BoardToPlayerTurnUnsuccessful(String player, List<CardData> penaltyCards,
                                              String gameId, String gameName) {
        Log.d("CommunicationTest", "BoardToPlayerTurnUnsuccessful");
    }


    /**
     * <b>BoardToPlayerGameOver</b> is called when the current player finished a successful move
     * and finished all of their cards - this player has won the game or when the board
     * wants to finish the game early (by clicking on end game button)
     * A message with the identity of the winner is sent to all the players.
     *
     * @param winners The players that have won the game.
     * @param players The players that are playing the game
     * @param gameId  The Id of the game
     */
    public void BoardToPlayerGameOver(List<String> winners, List<String> players,
                                      String gameId, String gameName) {
        Log.d("CommunicationTest", "BoardToPlayerGameOver");
    }

    /**
     * <b>BoardToPlayerGameStoppedUnexpectedly</b> is called the board leaves the game unexpectedly
     *
     * @param gameId         the Id of the game that was stopped
     * @param gameName       the name of the game that was stopped
     * @param invitedPlayers The name of the players that belong to this game
     */
    public void BoardToPlayerGameStoppedUnexpectedly(String gameId, String gameName,
                                                     List<String> invitedPlayers) {
        Log.d("CommunicationTest", "BoardToPlayerGameStoppedUnexpectedly");
    }

    /**
     * <b>BoardToPlayerCancelInvitation</b> is called the board cancels an invitation sent to
     * a player
     *
     * @param gameId     the Id of the game canceling the invitation
     * @param gameName   the name of the game canceling the invitation
     * @param playerName The name of hte player that his invitation is canceled
     */
    public void BoardToPlayerCancelInvitation(String gameId, String gameName,
                                              String playerName) {
        Log.d("CommunicationTest", "BoardToPlayerCancelInvitation");
    }


/*****************************************BOARD TO PLAYER - END*******************************/

/*****************************************PLAYER TO BOARD - START*******************************/

    /**
     * <b>PlayerToBoardNewCards</b> is called when the current player sends cards to
     * the board.
     *
     * @param cardsData  The cards sent to the board.
     * @param gameId     The Id of the game canceling the invitation
     * @param gameName   The name of the game canceling the invitation
     * @param playerName The name of the player sending the cards
     */
    public void PlayerToBoardNewCards(List<CardData> cardsData, String gameId, String gameName,
                                      String playerName) {
        Log.d("CommunicationTest", "PlayerToBoardNewCards");
    }

    /**
     * <b>PlayerToBoardJoinGame</b> is called when the player accepts an invitation sent
     * from the board and he is joining the game
     *
     * @param gameId     The Id of the game that it's invitation is accepted
     * @param gameName   The name of the game that it's invitation is accepted
     * @param playerName The name of the player that accepts the invitation
     */
    public void PlayerToBoardJoinGame(String gameId, String gameName, String playerName) {
        Log.d("CommunicationTest", "PlayerToBoardJoinGame");
    }

    /**
     * <b>PlayerToBoardDeclineGame</b> is called when the player declined an invitation sent
     * from the board and he is joining the game
     *
     * @param gameId     The Id of the game that it's invitation is declined
     * @param gameName   The name of the game that it's invitation is declined
     * @param playerName The name of the player that declines the invitation
     */
    public void PlayerToBoardDeclineGame(String gameId, String gameName, String playerName) {
        Log.d("CommunicationTest", "PlayerToBoardDeclineGame");
    }

    /**
     * <b>PlayerToBoardLeaveGame</b> is called when the player leaves the game after he
     * was invited by a board
     *
     * @param gameId     The Id of the game that invited the player
     * @param gameName   The name of the game that invited the player
     * @param playerName The name of the player that leaves the game
     */
    public void PlayerToBoardLeaveGame(String gameId, String gameName, String playerName) {
        Log.d("CommunicationTest", "PlayerToBoardLeaveGame");
    }

/*****************************************PLAYER TO BOARD - END*******************************/

}
