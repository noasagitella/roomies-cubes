package com.nse.roomiescubes.player.opening;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.nse.roomiescubes.ApplicationData;
import com.nse.roomiescubes.BaseActivity;
import com.nse.roomiescubes.OpeningActivity;
import com.nse.roomiescubes.R;
import com.nse.roomiescubes.player.game.PlayerMainGameActivity;
import com.parse.ParseInstallation;
import com.parse.ParseUser;

import org.json.JSONObject;

public class PlayerWaitForInvitation extends BaseActivity implements View.OnClickListener {

    Button joinGameButtonId_;
    Button declineGameButtonId_;
    TextView joinGameTextViewId_;
    ParseInstallation installation_;
    ParseUser currentUser_;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d("Player_startGame", "created intent");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_player_wait_for_invitation);
        ApplicationData.invitationClass = this;

        joinGameButtonId_ = (Button) findViewById(R.id.b_join_game);
        joinGameButtonId_.setOnClickListener(this);
        joinGameButtonId_.setVisibility(View.GONE);

        declineGameButtonId_ = (Button) findViewById(R.id.b_decline_game);
        declineGameButtonId_.setOnClickListener(this);
        declineGameButtonId_.setVisibility(View.GONE);

        joinGameTextViewId_ = (TextView) findViewById(R.id.tv_wait_for_invitation);

        installation_ = ParseInstallation.getCurrentInstallation();
        installation_.put("roleType", "player");
        installation_.saveInBackground();
        currentUser_ = ParseUser.getCurrentUser();
        if (!isNetworkAvailable()) {
            Toast.makeText(getApplicationContext(), "Error - No internet connection", Toast.LENGTH_LONG).show();
        }
    }


    DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            switch (which) {
                case DialogInterface.BUTTON_POSITIVE:
                    dialog.dismiss();
                    ApplicationData.communicationClass.PlayerToBoardLeaveGame(ApplicationData.gameId,
                            ApplicationData.gameName, ApplicationData.playerName);
                    ApplicationData.invitationClass = null;
                    ApplicationData.leaveGame();
                    Intent intent = new Intent(PlayerWaitForInvitation.this, OpeningActivity.class);
                    startActivity(intent);

                case DialogInterface.BUTTON_NEGATIVE:
                    dialog.dismiss();
                    break;
            }
        }
    };


    @Override
    public void onBackPressed() {
        if (ApplicationData.gameName != null) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Are you sure you you want to leave the game?").setPositiveButton("Yes", dialogClickListener)
                    .setNegativeButton("No", dialogClickListener).show();
        } else {
            ApplicationData.invitationClass = null;
            ApplicationData.leaveGame();
            Intent intent = new Intent(PlayerWaitForInvitation.this, OpeningActivity.class);
            startActivity(intent);
        }

    }

    public void onPushInvitation(String gameId, String gameName) {
        if (ApplicationData.gameId == null) {
            ApplicationData.gameName = gameName;
            ApplicationData.gameId = gameId;
            joinGameTextViewId_.setText("Hi " + ApplicationData.playerName + "! Game " + ApplicationData.gameName + " is inviting you to join game.");
            joinGameButtonId_.setVisibility(View.VISIBLE);
            declineGameButtonId_.setVisibility(View.VISIBLE);
        } else {
            ApplicationData.communicationClass.PlayerToBoardDeclineGame(gameId, gameName, ApplicationData.playerName);
            Log.d("wait for invitation", "game was invited twice");
        }

    }

    public void onPushGameStarts(JSONObject cardsData) {
        Toast.makeText(getApplicationContext(), "Game is starting!", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(this, PlayerMainGameActivity.class);
        intent.putExtra("cardsData", cardsData.toString());
        startActivity(intent);
    }

    @Override
    public void onClick(View v) {
        // join game or decline game buttons
        if (!isNetworkAvailable()) {
            Toast.makeText(getApplicationContext(), "Error - No internet connection", Toast.LENGTH_LONG).show();
            return;
        }

        Integer id = v.getId();
        switch (id) {
            case R.id.b_join_game:
                joinGameButtonId_.setVisibility(View.GONE);
                declineGameButtonId_.setVisibility(View.GONE);
                joinGameTextViewId_.setText("Game will soon start!");

                installation_.put("gameName", ApplicationData.gameName);
                installation_.saveInBackground();
                ApplicationData.communicationClass.PlayerToBoardJoinGame(ApplicationData.gameId, ApplicationData.gameName,
                        ApplicationData.playerName);
                break;
            case R.id.b_decline_game:
                joinGameButtonId_.setVisibility(View.GONE);
                declineGameButtonId_.setVisibility(View.GONE);
                joinGameTextViewId_.setText("Wait for invitation to join game");

                ApplicationData.communicationClass.PlayerToBoardDeclineGame(ApplicationData.gameId, ApplicationData.gameName, ApplicationData.playerName);
                ApplicationData.gameId = null;
                ApplicationData.gameName = null;
                break;
        }

    }

    public void onGameCanceledInvitation(String gameName, String gameId) {
        if (ApplicationData.gameId == null || !ApplicationData.gameId.equals(gameId)) {
            Log.d("wait for invitation", "game canceled invitation without sending one");
        } else { // play Joined game already
            Toast.makeText(getApplicationContext(), "Game " + gameName + " canceled the invitation",
                    Toast.LENGTH_SHORT).show();
            joinGameTextViewId_.setText("Wait for invitation to join game");
            joinGameButtonId_.setVisibility(View.GONE);
            declineGameButtonId_.setVisibility(View.GONE);
            ApplicationData.gameId = null;
            ApplicationData.gameName = null;
        }
    }

    @Override
    public void onDestroy() {
        ApplicationData.leaveGame();
        super.onDestroy();
    }


}
