package com.nse.roomiescubes.player.Ending;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.nse.roomiescubes.ApplicationData;
import com.nse.roomiescubes.OpeningActivity;
import com.nse.roomiescubes.R;

import java.util.ArrayList;

public class PlayerGameOverActivity extends Activity implements View.OnClickListener {
    private Button buttonPlayAgain_;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_player_game_over);

        buttonPlayAgain_ = (Button) findViewById(R.id.b_play_again);
        buttonPlayAgain_.setOnClickListener(this);

        TextView winner1 = (TextView) findViewById(R.id.v_winner1);
        TextView winner2 = (TextView) findViewById(R.id.v_winner2);
        ArrayList<String> winnersNames = getIntent().getStringArrayListExtra("winner");
        boolean isWinner = false;
        if (winnersNames.contains(ApplicationData.playerName)) {
            isWinner = true;
        }

        ApplicationData.leaveGame();
        if (isWinner) {
            winner1.setText("You are the winner! Well done!");
        } else if (winnersNames.size() == 1) {
            winner1.setText("The winner is:");
            winner2.setText(winnersNames.get(0));
        } else {
            // List is maximum size is 3 (because there are max 4 players, and current player not in list)
            winner1.setText("The winners are:");
            String msg = new String();
            for (int i = 0; i < winnersNames.size(); i++) {
                msg.concat(winnersNames.get(i));
                if (i != (winnersNames.size() - 1)) {
                    msg.concat(", ");
                } else {
                    msg.concat("!");
                }
            }
            winner2.setText(msg);
        }
    }


    @Override
    public void onClick(View v) {
        // play again button
        Intent intent = new Intent(this, OpeningActivity.class);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, OpeningActivity.class);
        startActivity(intent);
    }

    @Override
    public void onDestroy() {
        ApplicationData.leaveGame();
        super.onDestroy();
    }

}
