package com.nse.roomiescubes.player.game;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.DragEvent;
import android.view.View;
import android.widget.Button;
import android.widget.GridLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.nse.roomiescubes.OpeningActivity;
import com.nse.roomiescubes.ApplicationData;
import com.nse.roomiescubes.R;
import com.nse.roomiescubes.BaseActivity;
import com.nse.roomiescubes.components.CardsGrid;
import com.nse.roomiescubes.components.Deck;
import com.nse.roomiescubes.components.GameType;
import com.nse.roomiescubes.components.card.Card;
import com.nse.roomiescubes.components.card.CardData;
import com.nse.roomiescubes.components.card.PlayerCard;
import com.nse.roomiescubes.player.Ending.PlayerGameOverActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class PlayerMainGameActivity extends BaseActivity implements View.OnClickListener {
    private LinearLayout background_;
    private Button methodColors_;
    private Button methodValues_;
    private Button sendToBoard_;
    private Button leaveGame_;
    private TextView playerNameDisplay_;

    private CardsGrid cardsGrid_;
    private boolean playersTurn_ = false;
    private List<CardData> cardsSentThisTurn_;
    private List<CardData> cardsWaitingForEmptySpot_;

    // testing buttons and deck
    private Button addCard_;
    private Button setPlayerInitialCards_;
    private Button startPlayerTurn_;
    private Button endTurnSuccessfully_;
    private Button endTurnSuccessfullyTakeCard_;
    private Button endTurnWithPenalty_;
    private Deck deck_;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_player_main_game);

        setup();
        cardsGrid_ = new CardsGrid(this, (GridLayout) findViewById(R.id.gl_players_grid), GameType.PLAYER);

        ApplicationData.playerMainGameClass = this;
        if (!ApplicationData.testingMode) {
            try {
                JSONObject cardsDataJSON = new JSONObject(getIntent().getStringExtra("cardsData"));
                Boolean isPlayerTurn = cardsDataJSON.getBoolean("isPlayerTurn");
                if (isPlayerTurn) {
                    setPlayerInitialCardsAndStartPlayerTurn(CardData.getCardsDataListFromJSON(cardsDataJSON));
                } else {
                    setPlayerInitialCards(CardData.getCardsDataListFromJSON(cardsDataJSON));
                }
            } catch (JSONException var7) {
                Log.d("PlayerMainGame", "Unexpected JSONException when receiving push data: ");
            }
        }
        if(!isNetworkAvailable()) {
            Toast.makeText(getApplicationContext(), "Error - No internet connection", Toast.LENGTH_LONG).show();
        }

    }

    @Override
    public void onResume () {
        super.onResume();
        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
    }

    /**
     * Background drag listener
     */
    private class PlayerMainGameDragListener implements View.OnDragListener {
        @Override
        public boolean onDrag(View v, DragEvent event) {
            // handle the dragged view being dropped over a drop view
            View card = (View) event.getLocalState();
            switch (event.getAction()) {
                case DragEvent.ACTION_DROP:
                    card.setVisibility(View.VISIBLE);
                    break;
                default:
                    break;
            }
            return true;
        }
    }

    @Override
    public void onClick(View v) {
        Integer id = v.getId();

        switch (id) {
            case R.id.b_send_to_board: {
                if(!isNetworkAvailable()) {
                    Toast.makeText(getApplicationContext(), "Error - No internet connection", Toast.LENGTH_LONG).show();
                    return;
                }
                sendCardsToBoard();
                break;
            }
            case R.id.b_Leave_Game: {
                if(!isNetworkAvailable()) {
                    Toast.makeText(getApplicationContext(), "Error - No internet connection", Toast.LENGTH_LONG).show();
                    return;
                }
                leaveGameAlertDialog();
                break;
            }
            case R.id.b_arrange_by_colors: {
                List<Card> cards = cardsGrid_.getCardsList();
                Collections.sort(cards, Card.cardColorThanValueComparator);
                Card[] sortedCards = cardsGrid_.cardsListToArray(cards);
                cardsGrid_.updateGrid(sortedCards);
                break;
            }
            case R.id.b_arrange_by_values: {
                List<Card> cards = cardsGrid_.getCardsList();
                Collections.sort(cards, Card.cardValueThanColorComparator);
                Card[] sortedCards = cardsGrid_.cardsListToArray(cards);
                cardsGrid_.updateGrid(sortedCards);
                break;
            }

            // testing buttons
            case R.id.b_temp_add_card: {
                placeNewCard(deck_.takeCard());
                break;
            }
            case R.id.b_temp_set_initial_cards: {
                setPlayerInitialCards(deck_.takeKOrLessCards(14));
                break;
            }
            case R.id.b_temp_start_player_turn: {
                startPlayerTurn();
                break;
            }
            case R.id.b_temp_end_succc: {
                endTurnSuccessfully(cardsSentThisTurn_);
                break;
            }
            case R.id.b_temp_end_succc_with_card: {
                endTurnSuccessfully(deck_.takeCard());
                break;
            }
            case R.id.b_temp_end_penalty: {
                endTurnWithPenalty(deck_.takeKOrLessCards(3));
                break;
            }
            default:
                break;
        }
    }


    DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            switch (which) {
                case DialogInterface.BUTTON_POSITIVE:
                    dialog.dismiss();
                    ApplicationData.communicationClass.PlayerToBoardLeaveGame(ApplicationData.gameId,
                            ApplicationData.gameName, ApplicationData.playerName);
                    ApplicationData.leaveGame();
                    ApplicationData.playerMainGameClass = null;
                    Intent intent = new Intent(PlayerMainGameActivity.this, OpeningActivity.class);
                    startActivity(intent);
                    //finish();
                    break;

                case DialogInterface.BUTTON_NEGATIVE:
                    dialog.dismiss();
                    break;
            }
        }
    };

    @Override
    public void onBackPressed() {
        leaveGameAlertDialog();
    }

    @Override
    public void onDestroy() {
        ApplicationData.leaveGame();
        super.onDestroy();
    }


    /**
     * <b>startPlayerTurn</b>
     * Player gets notified that his turn starts now and can send cards to the board.
     */
    public void startPlayerTurn() {
        Toast.makeText(this.getApplicationContext(), "Your turn to play!", Toast.LENGTH_SHORT).show();
        playersTurn_ = true;
        sendToBoard_.setEnabled(true);
    }

    /**
     * <b>setPlayerInitialCards</b> adds the given cards to the players grid.
     *
     * @param cardsDataList list of cards to be handed to the player at the beginning of the game
     */
    public void setPlayerInitialCards(List<CardData> cardsDataList) {
        placeNewCards(cardsDataList);
    }

    /**
     * <b>startPlayerTurn</b> adds the given cards to the players grid and
     * Player gets notified that his turn starts now and can send cards to the board.
     *
     * @param cardsDataList list of cards to be handed to the player at the beginning of the game
     */
    public void setPlayerInitialCardsAndStartPlayerTurn(List<CardData> cardsDataList) {
        setPlayerInitialCards(cardsDataList);
        startPlayerTurn();
    }

    /**
     * <b>endTurnSuccessfully</b>
     * Player gets notified that his turn was ended successfully -
     * all the cards sent to the board were placed correctly.
     *
     * @cardsPlaced the cards the player has placed on the board successfully
     */
    public void endTurnSuccessfully(List<CardData> cardsPlaced) {
        cardsSentThisTurn_.removeAll(cardsPlaced);
        placeNewCards(cardsSentThisTurn_); // Player sent cards put these were not placed on board yet - OK!
        endPlayerTurn();
        Toast.makeText(this.getApplicationContext(), "Successful turn!", Toast.LENGTH_SHORT).show();
    }

    /**
     * <b>endTurnSuccessfully</b>
     * Player gets notified that his turn was ended and the card
     * he took from deck appears on his rack.
     */
    public void endTurnSuccessfully(CardData cardData) {
        // THE PLAYER DIDN'T PLACE ANY CARDS ON THE BOARD!
        placeNewCards(cardsSentThisTurn_); // Player sent cards put these were not placed on board yet - OK!
        endPlayerTurn();
        placeNewCard(cardData);
        Toast.makeText(this.getApplicationContext(), "Turn ended successfully!", Toast.LENGTH_SHORT).show();
    }

    /**
     * <b>endTurnWithPenalty</b>
     * Player gets notified that his turn was ended with penalty and the penalty cards
     * together with the cards he placed on board appear on his rack.
     */
    public void endTurnWithPenalty(List<CardData> penaltyCardsData) {
        placeNewCards(cardsSentThisTurn_);
        endPlayerTurn();
        placeNewCards(penaltyCardsData);
        Toast.makeText(this.getApplicationContext(), "Turn ended and you receive 3 new cards as penalty!", Toast.LENGTH_SHORT).show();
    }

    /**
     * <b>finishGame</b>
     * Player gets notified that game is finished and the identity of the winners.
     */
    public void finishGame(List<String> winnersNames) {
        Log.d("PlayerMainGameActivity", "finishGame");
        Intent intent = new Intent(this, PlayerGameOverActivity.class);
        ApplicationData.playerMainGameClass = null;
        intent.putStringArrayListExtra("winner", (ArrayList<String>) winnersNames);
        startActivity(intent);
    }

    /**
     * <b>gameStoppedUnexpectedly</b>
     * Player gets notified that the game has stopped.
     */
    public void gameStoppedUnexpectedly() {
        Toast.makeText(getApplicationContext(), "The board left the game :(", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(PlayerMainGameActivity.this, OpeningActivity.class);
        startActivity(intent);
    }

    private void setup() {
        background_ = (LinearLayout) findViewById(R.id.player_main_game_layout);
        background_.setOnDragListener(new PlayerMainGameDragListener());

        methodColors_ = (Button) findViewById(R.id.b_arrange_by_colors);
        methodColors_.setOnClickListener(this);

        methodValues_ = (Button) findViewById(R.id.b_arrange_by_values);
        methodValues_.setOnClickListener(this);

        sendToBoard_ = (Button) findViewById(R.id.b_send_to_board);
        sendToBoard_.setOnClickListener(this);
        sendToBoard_.setEnabled(false);
        cardsSentThisTurn_ = new ArrayList<>();
        cardsWaitingForEmptySpot_ = new ArrayList<>();

        leaveGame_ = (Button) findViewById(R.id.b_Leave_Game);
        leaveGame_.setOnClickListener(this);

        playerNameDisplay_ = (TextView) findViewById(R.id.tv_player_name);
        playerNameDisplay_.setText(ApplicationData.playerName);

        // testing buttons and deck
        addCard_ = (Button) findViewById(R.id.b_temp_add_card);
        setPlayerInitialCards_ = (Button) findViewById(R.id.b_temp_set_initial_cards);
        startPlayerTurn_ = (Button) findViewById(R.id.b_temp_start_player_turn);
        endTurnSuccessfully_ = (Button) findViewById(R.id.b_temp_end_succc);
        endTurnSuccessfullyTakeCard_ = (Button) findViewById(R.id.b_temp_end_succc_with_card);
        endTurnWithPenalty_ = (Button) findViewById(R.id.b_temp_end_penalty);
        deck_ = new Deck();
        if (ApplicationData.testingMode) {
            addCard_.setOnClickListener(this);
            setPlayerInitialCards_.setOnClickListener(this);
            startPlayerTurn_.setOnClickListener(this);
            endTurnSuccessfully_.setOnClickListener(this);
            endTurnSuccessfullyTakeCard_.setOnClickListener(this);
            endTurnWithPenalty_.setOnClickListener(this);
        } else {
            addCard_.setVisibility(View.GONE);
            setPlayerInitialCards_.setVisibility(View.GONE);
            startPlayerTurn_.setVisibility(View.GONE);
            endTurnSuccessfully_.setVisibility(View.GONE);
            endTurnSuccessfullyTakeCard_.setVisibility(View.GONE);
            endTurnWithPenalty_.setVisibility(View.GONE);
        }
    }

    /**
     * Sends the selected cards to the board.
     * - if there are no selected cards shows an error message
     */
    private void sendCardsToBoard() {
        List<CardData> selectedCardsDataList = new ArrayList<>();
        Card[] cards = cardsGrid_.asArray();
        for (int i = 0; i < cards.length; i++) {
            if (cards[i] != null) {
                if (((PlayerCard) cards[i]).isPlayerCardSelected()) {
                    selectedCardsDataList.add(cards[i].getCardData());
                    // Delete the cards that the player sent to the board
                    cardsGrid_.removeCard(i);
                }
            }
        }

        if (selectedCardsDataList.isEmpty()) {
            Toast.makeText(this.getApplicationContext(), "No cards were selected!", Toast.LENGTH_SHORT);
            return;
        }

        cardsSentThisTurn_.addAll(selectedCardsDataList);
        ApplicationData.communicationClass.PlayerToBoardNewCards(selectedCardsDataList, ApplicationData.gameId,
                ApplicationData.gameName,
                ApplicationData.playerName);

    }

    private void placeNewCards(List<CardData> newCardsDataList) {
        for (CardData cardData : newCardsDataList) {
            placeNewCard(cardData);
        }
    }

    private void placeNewCard(CardData newCardData) {
        try {
            cardsGrid_.addCard(new PlayerCard(this.getApplicationContext(), newCardData));
        } catch (Exception e) {
            if (e.getMessage().equals("CardsGrid is full")) {
                cardsWaitingForEmptySpot_.add(newCardData);
            } else {
                Toast.makeText(this.getApplicationContext(), "Error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void placeWaitingCards() {
        List<CardData> cardDatas = cardsWaitingForEmptySpot_;
        cardsWaitingForEmptySpot_ = new ArrayList<>();
        for (CardData cardData : cardDatas) {
            placeNewCard(cardData);
        }
    }

    private void endPlayerTurn() {
        if (!playersTurn_) {
            Toast.makeText(this.getApplicationContext(), "Error: Can't end turn - it's not the players turn!", Toast.LENGTH_SHORT).show();
        }
        playersTurn_ = false;
        sendToBoard_.setEnabled(false);
        cardsSentThisTurn_ = new ArrayList<>();
        placeWaitingCards();
    }

    private void leaveGameAlertDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Are you sure you you want to leave the game?").setPositiveButton("Yes", dialogClickListener)
                .setNegativeButton("No", dialogClickListener).show();
    }
}




