package com.nse.roomiescubes.player.opening;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.nse.roomiescubes.OpeningActivity;
import com.nse.roomiescubes.ApplicationData;
import com.nse.roomiescubes.R;

public class PlayerStartGameActivity extends Activity implements View.OnClickListener {
    Button buttonSignUp_;
    Button buttonLogIn_;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_player_start_game);
        buttonSignUp_ = (Button) findViewById(R.id.b_sign_up);
        buttonSignUp_.setOnClickListener(this);
        buttonLogIn_ = (Button) findViewById(R.id.b_log_in);
        buttonLogIn_.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent intent;
        Integer id = v.getId();
        switch (id) {
            case R.id.b_log_in:
                Log.d("PlayerStartGameActivity", "wants to log in");
                intent = new Intent(this, PlayerLogInActivity.class);
                startActivity(intent);
                break;
            case R.id.b_sign_up:
                Log.d("PlayerStartGameActivity", "wants to sign up");
                intent = new Intent(this, PlayerSignUpActivity.class);
                startActivity(intent);
                break;
            default:
                break;

        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, OpeningActivity.class);
        startActivity(intent);
    }

    @Override
    public void onDestroy() {
        ApplicationData.leaveGame();
        super.onDestroy();
    }


}
