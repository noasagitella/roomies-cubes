package com.nse.roomiescubes.player.opening;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.nse.roomiescubes.ApplicationData;
import com.nse.roomiescubes.R;
import com.nse.roomiescubes.BaseActivity;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.parse.SignUpCallback;

public class PlayerSignUpActivity extends BaseActivity implements View.OnClickListener {

    private EditText editTextUsername;
    private EditText editTextPassword;
    private EditText editTextPasswordVerification;
    private EditText editTextEmail;
    private Button buttonSignUp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_player_sign_up);
        Log.d("Player_SignUp", "started activity");
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        editTextUsername = (EditText) findViewById(R.id.username);

        editTextPassword = (EditText) findViewById(R.id.password);

        editTextPasswordVerification = (EditText) findViewById(R.id.verifyPassword);

        editTextEmail = (EditText) findViewById(R.id.email);

        buttonSignUp = (Button) findViewById(R.id.b_signUp);
        buttonSignUp.setOnClickListener(this);
        if(!isNetworkAvailable()) {
            Toast.makeText(getApplicationContext(), "Error - No internet connection", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onClick(View v) {
        // sign up button
        hideSoftKeyboard();
        if(!isNetworkAvailable()) {
            Toast.makeText(getApplicationContext(), "Error - No internet connection", Toast.LENGTH_LONG).show();
            return;
        }

        buttonSignUp.setClickable(false);
        final String s_username;
        String s_password;
        String s_passwordVerification;
        String s_email;
        // empty string, don't parse as int.
        s_username = editTextUsername.getText().toString();
        s_password = editTextPassword.getText().toString();
        s_passwordVerification = editTextPasswordVerification.getText().toString();
        s_email = editTextEmail.getText().toString();

        Log.d("Player_SignUp", "username: " + s_username);
        Log.d("Player_SignUp", "password: " + s_password);
        Log.d("Player_SignUp", "passwordVerification: " + s_passwordVerification);
        Log.d("Player_SignUp", "email: " + s_email);
        if (!s_password.equals(s_passwordVerification)) {
            editTextPassword.setText("");
            editTextPasswordVerification.setText("");
            Toast.makeText(getApplicationContext(), "Password was not verified - please insert again", Toast.LENGTH_LONG).show();
            buttonSignUp.setClickable(true);
            return;
        }
        ParseUser user;
        user = new ParseUser();
        if(s_username.equals("")) {
            Toast.makeText(getApplicationContext(), "Name is missing", Toast.LENGTH_LONG).show();
            buttonSignUp.setClickable(true);
            return;
        }
        if(s_password.equals("")) {
            Toast.makeText(getApplicationContext(), "Password is missing", Toast.LENGTH_LONG).show();
            buttonSignUp.setClickable(true);
            return;
        }
        if(s_email.equals("")) {
            Toast.makeText(getApplicationContext(), "Email is missing", Toast.LENGTH_LONG).show();
            buttonSignUp.setClickable(true);
            return;
        }
        user.put("logged_in", true);
        user.setEmail(s_email);
        user.setPassword(s_password);
        user.setUsername(s_username);
        user.signUpInBackground(new SignUpCallback() {
            public void done(ParseException e) {
                if (e == null) {
                    Log.d("Player_SignUp", "signed up successful");
                    ParseInstallation installation = ParseInstallation.getCurrentInstallation();
                    ParseUser user = ParseUser.getCurrentUser();
                    Log.d("Player_SignUp", "Current user is: " + user.getUsername());
                    installation.put("userID", user); //Update user column in installation object
                    installation.put("userName", s_username); //Update user column in installation object
                    installation.saveInBackground();
                    Log.d("playerSignUp", "installation is: " + installation.getInstallationId());
                    installation.saveInBackground(new SaveCallback() {
                        public void done(ParseException e) {
                            if (e == null) {
                                Log.d("playerSignUp", "saved successfully in installation");
                            } else {
                                Log.d("playerSignUp", "not saved in installation. error is: " + e.getMessage());
                            }
                        }
                    });
                    ApplicationData.playerName = s_username;
                    Intent intent = new Intent(PlayerSignUpActivity.this, PlayerWaitForInvitation.class);
                    startActivity(intent);
                    buttonSignUp.setClickable(true);

                } else {
                    if(e.getCode() == 125) {
                        Toast.makeText(getApplicationContext(), "Invalid email", Toast.LENGTH_LONG).show();
                        buttonSignUp.setClickable(true);
                    }
                    else if(e.getCode() == 202) {
                        Toast.makeText(getApplicationContext(), "A user with this name already exists, " +
                                "please insert a different name", Toast.LENGTH_LONG).show();
                        buttonSignUp.setClickable(true);
                    }
                    else if(e.getCode() == 203) {
                        Toast.makeText(getApplicationContext(), "This email address already exists, " +
                                "please insert a different email address", Toast.LENGTH_LONG).show();
                        buttonSignUp.setClickable(true);
                    }
                    else {
                        Toast.makeText(getApplicationContext(), "There was a problem signing in, " +
                                "please try again", Toast.LENGTH_LONG).show();
                        buttonSignUp.setClickable(true);
                    }


                }
            }
        });

    }

    @Override
    public void onDestroy() {
        ApplicationData.leaveGame();
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {

        Intent intent = new Intent(this, PlayerStartGameActivity.class);
        startActivity(intent);
    }

}
