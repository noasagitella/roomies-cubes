package com.nse.roomiescubes.player.opening;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.nse.roomiescubes.ApplicationData;
import com.nse.roomiescubes.BaseActivity;
import com.nse.roomiescubes.R;
import com.parse.FindCallback;
import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.util.List;

public class PlayerLogInActivity extends BaseActivity implements View.OnClickListener {

    private EditText editTextUsername_;
    private EditText editTextPassword_;
    private Button buttonLogIn_;
    String s_password;
    String s_username;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_player_log_in);

        editTextUsername_ = (EditText) findViewById(R.id.e_username);
        editTextPassword_ = (EditText) findViewById(R.id.e_password);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);

        buttonLogIn_ = (Button) findViewById(R.id.b_log_in);
        buttonLogIn_.setOnClickListener(this);
        if (!isNetworkAvailable()) {
            Toast.makeText(getApplicationContext(), "Error - No internet connection", Toast.LENGTH_LONG).show();
        }
    }


    @Override
    public void onClick(View v) {
        // log in button
        hideSoftKeyboard();
        if (!isNetworkAvailable()) {
            Toast.makeText(getApplicationContext(), "Error - No internet connection", Toast.LENGTH_LONG).show();
            return;
        }

        buttonLogIn_.setClickable(false);

        // empty string, don't parse as int.
        s_username = editTextUsername_.getText().toString();
        s_password = editTextPassword_.getText().toString();

        Log.d("Player_LogIn", "username: " + s_username);
        Log.d("Player_LogIn", "password: " + s_password);

        ParseQuery query = ParseUser.getQuery();
        query.whereEqualTo("username", s_username);
        query.findInBackground(new FindCallback<ParseUser>() {
            public void done(List<ParseUser> objects, ParseException e) {
                if (e == null) {
                    if (objects.isEmpty()) {
                        Toast.makeText(getApplicationContext(), "Player " + s_username + " does not exist", Toast.LENGTH_SHORT).show();
                    } else if ((boolean) objects.get(0).get("logged_in") == true) {
                        Toast.makeText(getApplicationContext(), "Player " + s_username + " is already playing", Toast.LENGTH_SHORT).show();
                    } else {
                        ParseUser.logInInBackground(s_username, s_password, new LogInCallback() {
                            public void done(ParseUser user, ParseException e) {
                                if (user != null) {
                                    user.put("logged_in", true);
                                    user.saveInBackground();
                                    ApplicationData.playerName = s_username;
                                    ParseInstallation installation = ParseInstallation.getCurrentInstallation();
                                    //installation.put("userID",   objects.get(0)); //Update user column in installation object
                                    installation.put("userName", s_username);
                                    installation.put("userID", user);
                                    installation.saveInBackground(new SaveCallback() {
                                        public void done(ParseException e) {
                                            if (e == null) {
                                                Log.d("Player_LogIn", "saved successfully in installation");
                                            } else {
                                                Log.d("Player_LogIn", "not saved in installation. error is: " + e.getMessage());
                                            }
                                        }
                                    });
                                    Intent intent = new Intent(PlayerLogInActivity.this, PlayerWaitForInvitation.class);
                                    startActivity(intent);
                                    buttonLogIn_.setClickable(true);
                                    //finish();
                                } else {
                                    Log.d("Player_LogIn", "not saved" + e.getMessage());
                                    Toast.makeText(getApplicationContext(), "The password or " +
                                            "username are incorrect "
                                            , Toast.LENGTH_SHORT).show();
                                    buttonLogIn_.setClickable(true);
                                }
                            }
                        });
                    }
                    buttonLogIn_.setClickable(true);
                } else {
                    Log.d("Player_LogIn", "the query couldn't be complited");
                    Log.d("Opening activity", "not saved in installation. error is: " + e.getMessage());
                    Toast.makeText(getApplicationContext(), "There was a connection problem" +
                            ", please try again ", Toast.LENGTH_SHORT).show();
                    buttonLogIn_.setClickable(true);
                }
            }
        });
    }

    @Override
    public void onDestroy() {
        ApplicationData.leaveGame();
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, PlayerStartGameActivity.class);
        startActivity(intent);
    }


}
